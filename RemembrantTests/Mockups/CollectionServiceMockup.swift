//
//  CollectionServiceMockup.swift
//  RemembrantTests
//
//  Created by Konstantin Tukmakov on 09.01.2023.
//

import Foundation
@testable import Remembrant

final class CollectionServiceMockup: CollectionProvider {

    var mockedCollections = [
        Collection(id: 1, title: "Some", flashcards: []),
        Collection(id: 14, title: "Another some", flashcards: [])
    ]

    func getCollections(completion: @escaping (() throws -> [Remembrant.Collection]?) -> Void) {
        completion({ mockedCollections })
    }

    func updateCollection(_ collection: Remembrant.Collection, completion: @escaping (() throws -> Remembrant.Collection) -> Void) {

    }

    func storeCollection(_ collection: Remembrant.Collection, completion: @escaping (() throws -> Remembrant.Collection) -> Void) {
        completion({ Collection(id: 10, title: collection.title, flashcards: []) })
    }

    func deleteCollection(_ collection: Remembrant.Collection, completion: @escaping (() throws -> Void) -> Void) {

        guard let index = mockedCollections.firstIndex(of: collection) else {
            completion({ throw TestError.error })
            return
        }

        mockedCollections.remove(at: index)
        completion({})
    }

}
