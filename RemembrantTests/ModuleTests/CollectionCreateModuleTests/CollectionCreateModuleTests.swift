//
//  CollectionCreateModuleTests.swift
//  RemembrantTests
//
//  Created by Konstantin Tukmakov on 09.01.2023.
//

import XCTest
@testable import Remembrant

final class CollectionCreateModuleTests: XCTestCase {

    var navigationController: UINavigationController?

    override func setUpWithError() throws {
        navigationController = UINavigationController()
    }

    func testConfigurator() throws {
        // given
        let module = CollectionCreateModule()
        let moduleInput = CollectionCreateModuleInput(onDismiss: onDismiss)

        // when
        let view = module.create(for: navigationController, moduleInput: moduleInput)

        // then
        guard let view = view as? CollectionCreateView else {
            XCTFail("CollectionCreateModule must return CollectionCreateView instance")
            return
        }

        XCTAssertNotNil(view.output)
    }

    func onDismiss(_ collection: Collection) {

    }
}
