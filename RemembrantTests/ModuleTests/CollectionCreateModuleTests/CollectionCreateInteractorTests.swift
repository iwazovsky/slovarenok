//
//  CollectionCreateInteractorTests.swift
//  RemembrantTests
//
//  Created by Konstantin Tukmakov on 09.01.2023.
//

import XCTest
@testable import Remembrant

final class CollectionCreateInteractorTests: XCTestCase {

    var provider: CollectionServiceMockup?
    var presenter: PresenterMockup?

    override func setUpWithError() throws {
        provider = CollectionServiceMockup()
        presenter = PresenterMockup()
    }

    func testCreateCollection() {
        // given
        let interactor = CollectionCreateInteractor()
        interactor.output = presenter
        interactor.collectionProvider = provider

        // when
        interactor.createCollection(title: "Some title")

        // then
        XCTAssertNotNil(presenter?.createdCollection)
        XCTAssertEqual(presenter?.createdCollection?.title, "Some title")
        XCTAssertEqual(presenter?.createdCollection?.id, 10)
    }

    final class PresenterMockup: CollectionCreateInteractorOutput {
        var createdCollection: Collection?

        func collectionCreated(collection: Remembrant.Collection) {
            createdCollection = collection
        }

    }
}
