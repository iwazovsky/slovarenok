//
//  CollectionCreatePresenterTests.swift
//  RemembrantTests
//
//  Created by Konstantin Tukmakov on 09.01.2023.
//

import XCTest
@testable import Remembrant

final class CollectionCreatePresenterTests: XCTestCase {

    var view: ViewMockup!
    var router: RouterMockup!
    var interactor: InteractorMockup!

    override func setUpWithError() throws {
        view = ViewMockup()
        router = RouterMockup()
        interactor = InteractorMockup()
    }

    func testCreateCollection() {
        // given
        let expectation = expectation(description: "Success")

        let presenter = CollectionCreatePresenter()
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        interactor.output = presenter

        // when
        presenter.createCollection(title: "Some title")

        // then
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            XCTAssertNotNil(self.router.collection)
            XCTAssertEqual(self.router.collection?.id, 10)
            XCTAssertEqual(self.router.collection?.title, "Some title")

            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 0.5)
    }

    func testCancelCreate() {
        // given
        let presenter = CollectionCreatePresenter()
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router

        // when
        presenter.cancelCreate()

        // then
        XCTAssertTrue(router.cancelExecuted)
    }

    final class RouterMockup: CollectionCreateRouterInput {
        var cancelExecuted = false
        var collection: Collection?

        func closeModal(collection: Remembrant.Collection) {
            self.collection = collection
        }

        func cancel() {
            cancelExecuted = true
        }
    }

    final class InteractorMockup: CollectionCreateInteractorInput {
        var output: CollectionCreateInteractorOutput!

        func createCollection(title: String) {
            output.collectionCreated(collection: Collection(id: 10, title: title, flashcards: []))
        }
    }

    final class ViewMockup: CollectionCreateViewInput {
        func collectionCreated() {

        }
    }
}
