//
//  DashboardModuleTests.swift
//  RemembrantTests
//
//  Created by Konstantin Tukmakov on 09.01.2023.
//

import XCTest
@testable import Remembrant

final class DashboardModuleTests: XCTestCase {

    var dashboardModule: DashboardModule?

    override func setUpWithError() throws {
        dashboardModule = DashboardModule()
    }

    func testConfigurator() throws {
        // given
        let navigationController = UINavigationController()

        // when
        let view = dashboardModule?.create(for: navigationController, moduleInput: nil)

        // then
        guard let view = view as? DashboardView else {
            XCTFail("DashboardModule must return DashboardView instance")
            return
        }
        XCTAssertNotNil(view.output)
        XCTAssertNotNil(view.greetingRandomizer)
    }

}
