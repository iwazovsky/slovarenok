//
//  DashboardPresenterTests.swift
//  RemembrantTests
//
//  Created by Konstantin Tukmakov on 09.01.2023.
//

import XCTest
@testable import Remembrant

final class DashboardPresenterTests: XCTestCase {

    var view: ViewMockup!
    var interactor: InteractorMockup!
    var router: RouterMockup!

    override func setUpWithError() throws {
        view = ViewMockup()
        interactor = InteractorMockup()
        router = RouterMockup()
    }

    func testNavigateToCollections() {
        // given
        let presenter = DashboardPresenter()
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router

        // when
        presenter.navigateToCollections()

        // then
        XCTAssertTrue(router.navigateToCollectionsExecuted)
    }

    func testNavigateToSettings() {
        // given
        let presenter = DashboardPresenter()
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router

        // when
        presenter.navigateToSettings()

        // then
        XCTAssertTrue(router.navigateToSettingsExecuted)
    }

    final class ViewMockup: DashboardViewInput {

    }

    final class InteractorMockup: DashboardInteractorInput {

    }

    final class RouterMockup: DashboardRouterInput {
        var navigateToCollectionsExecuted = false
        var navigateToSettingsExecuted = false

        func navigateToCollections() {
            navigateToCollectionsExecuted = true
        }

        func navigateToSettings() {
            navigateToSettingsExecuted = true
        }

    }
}
