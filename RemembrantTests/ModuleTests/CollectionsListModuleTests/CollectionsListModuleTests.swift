//
//  CollectionsListModuleTests.swift
//  RemembrantTests
//
//  Created by Konstantin Tukmakov on 09.01.2023.
//

import XCTest
@testable import Remembrant

final class CollectionsListModuleTests: XCTestCase {

    var collectionsListModule: CollectionsListModule?

    override func setUpWithError() throws {
        collectionsListModule = CollectionsListModule()
    }

    func testConfigurator() throws {
        // given
        let navigationController = UINavigationController()

        // when
        let view = collectionsListModule?.create(for: navigationController, moduleInput: nil)

        // then
        guard let view = view as? CollectionsListView else {
            XCTFail("CollectionsListModule must return CollectionsListView instance")
            return
        }

        XCTAssertNotNil(view.output)
        XCTAssertNotNil(view.collectionsListTableViewAdapter)
    }

}
