//
//  CollectionsListInteractorTests.swift
//  RemembrantTests
//
//  Created by Konstantin Tukmakov on 09.01.2023.
//

import XCTest
@testable import Remembrant

final class CollectionsListInteractorTests: XCTestCase {

    var output: InteractorOutputMockup?
    var collectionService: CollectionServiceMockup?

    override func setUpWithError() throws {
        output = InteractorOutputMockup()
        collectionService = CollectionServiceMockup()
    }

    func testFetchCollections() throws {
        // given
        let successExpectation = expectation(description: "Success")

        let interactor = CollectionsListInteractor()
        interactor.output = output
        interactor.collectionService = collectionService

        // when
        interactor.fetchCollections()

        // then
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            XCTAssertNotNil(self.output?.collections)
            XCTAssertEqual(self.output?.collections, self.collectionService?.mockedCollections)

            successExpectation.fulfill()
        }

        wait(for: [successExpectation], timeout: 1)
    }

    func testDeleteCollection() throws {
        // given
        let successExpectation = expectation(description: "Success")

        let interactor = CollectionsListInteractor()
        interactor.output = output
        interactor.collectionService = collectionService
        output?.collections = collectionService?.mockedCollections

        var resultCollections = collectionService?.mockedCollections
        resultCollections?.remove(at: 0)

        // when
        guard let collection = collectionService?.mockedCollections.first else {
            XCTFail("Mocked up collection service is not implemented right")
            throw TestError.error
        }
        interactor.deleteCollection(collection)

        // then
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            XCTAssertNotNil(self.collectionService?.mockedCollections)
            XCTAssertEqual(self.output?.collections, resultCollections!)

            successExpectation.fulfill()
        }

        wait(for: [successExpectation], timeout: 1)
    }

    final class InteractorOutputMockup: CollectionsListInteractorOutput {

        var collections: [Collection]?

        func onFetchCollections(_ collections: [Remembrant.Collection]) {
            self.collections = collections
        }

        func onDeletedCollection(_ collection: Remembrant.Collection) {
            self.collections?.remove(at: 0)
        }
    }
}
