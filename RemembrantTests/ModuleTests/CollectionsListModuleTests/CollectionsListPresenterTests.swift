//
//  CollectionsListPresenterTests.swift
//  RemembrantTests
//
//  Created by Konstantin Tukmakov on 09.01.2023.
//

import XCTest
@testable import Remembrant

final class CollectionsListPresenterTests: XCTestCase {

    var view: ViewMockup!
    var interactor: InteractorMockup!
    var router: RouterMockup!

    override func setUpWithError() throws {
        view = ViewMockup()
        interactor = InteractorMockup()
        router = RouterMockup()
    }

    func testFetchCollections() {
        // given
        let presenter = CollectionsListPresenter()
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router

        interactor.output = presenter

        // when
        presenter.fetchCollections()

        // then
        XCTAssertNotNil(presenter.collections)
        XCTAssertEqual(presenter.collections, interactor.collections)
    }

    func testDeleteCollection() {
        // given
        let presenter = CollectionsListPresenter()
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router

        interactor.output = presenter
        let collectionToDelete = interactor.collections.first

        // when
        presenter.deleteCollection(collectionToDelete!)

        // then
        XCTAssertNotNil(presenter.collections)
        XCTAssertTrue(presenter.collections!.isEmpty)
    }

    func testNavigateToCollection() {
        // given
        let presenter = CollectionsListPresenter()
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router

        // when
        presenter.navigateToCollection(Collection(id: 0, title: "", flashcards: []))

        // then
        XCTAssertTrue(router.navigateToCollectionExecuted)
    }

    func testNavigateToCollectionCreate() {
        // given
        let presenter = CollectionsListPresenter()
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router

        // when
        presenter.onAddCollectionButtonClick()

        // then
        XCTAssertTrue(router.navigateToCollectionCreateExecuted)
    }

    final class InteractorMockup: CollectionsListInteractorInput {
        var output: CollectionsListInteractorOutput!
        var collections = [
            Collection(id: 0, title: "test", flashcards: [])
        ]

        func fetchCollections() {
            output.onFetchCollections(collections)
        }

        func deleteCollection(_ collection: Remembrant.Collection) {
            output.onDeletedCollection(collection)
        }
    }

    final class ViewMockup: CollectionsListViewInput {
        func navigateToCollection(_ collection: Remembrant.Collection) {

        }

        func hydrateCollectionsList(_ collections: [Remembrant.Collection]) {

        }

        func onNewCollection(_ collection: Remembrant.Collection) {

        }

        func deleteCollection(_ collection: Remembrant.Collection) {

        }

        func onDeletedCollection(_ collection: Remembrant.Collection) {

        }

        func collectionUpdated(_ collection: Remembrant.Collection) {

        }
    }

    final class RouterMockup: CollectionsListRouterInput {

        var navigateToCollectionExecuted = false
        var navigateToCollectionCreateExecuted = false

        func navigateToCollection(_ collection: Remembrant.Collection, onDismiss: @escaping Remembrant.CollectionDismissCallback) {
            navigateToCollectionExecuted = true
        }

        func navigateToCollectionCreate(onDismiss: @escaping Remembrant.CollectionCreateDismissCallback) {
            navigateToCollectionCreateExecuted = true
        }

    }
}
