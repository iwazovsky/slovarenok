//
//  CollectionServiceTests.swift
//  RemembrantTests
//
//  Created by Konstantin Tukmakov on 09.01.2023.
//

import XCTest
@testable import Remembrant

final class CollectionServiceTests: XCTestCase {

    // MARK: Dependencies
    var collectionNetworkService: СollectionNetworkServiceMockup!
    var collectionCacheService: CollectionCacheServiceMockup!

    var collections: [Collection]!

    override func setUpWithError() throws {
        collectionNetworkService = СollectionNetworkServiceMockup()
        collectionCacheService = CollectionCacheServiceMockup()

        collections = [
            Collection(id: 12, title: "Happy twelve"),
            Collection(id: 13, title: "Thirteen")
        ]

        collectionNetworkService.collections = collections
        collectionCacheService.collections = collections
    }

    func testStoreAction() {
        // given
        let expectation = expectation(description: "Success")

        let collectionService = CollectionService()
        collectionService.networkProvider = collectionNetworkService
        collectionService.cacheProvider = collectionCacheService

        let toInsert: Collection = Collection(id: 66, title: "One number to devil")
        collections.append(toInsert)

        // when
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            collectionService.storeCollection(toInsert) { (_ inner: CreateCollectionThrowableCallback) in

                // then
                do {
                    let collection = try inner()
                    XCTAssertNotNil(collection)
                    XCTAssertEqual(collection, toInsert)
                    XCTAssertEqual(self.collections, self.collectionNetworkService.collections)
                    XCTAssertEqual(self.collections, self.collectionCacheService.collections)
                } catch {
                    XCTFail("Inner thrown an error")
                }

                expectation.fulfill()
            }
        }

        wait(for: [expectation], timeout: 0.5)
    }

    func testDeleteCollection() {
        // given
        let expectation = expectation(description: "Success")
        let collectionService = CollectionService()
        collectionService.networkProvider = collectionNetworkService
        collectionService.cacheProvider = collectionCacheService

        let toDelete = collections.first!
        collections.remove(at: 0)

        // when
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            // then
            collectionService.deleteCollection(toDelete) { (_ inner: () throws -> Void) in
                do {
                    try inner()

                    XCTAssertNotNil(self.collectionNetworkService.collections)
                    XCTAssertEqual(self.collections, self.collectionNetworkService.collections)

                    XCTAssertNotNil(self.collectionCacheService.collections)
                    XCTAssertEqual(self.collections, self.collectionCacheService.collections)
                } catch {
                    XCTFail("Inner thrown unexpected error")
                }

                expectation.fulfill()
            }
        }

        wait(for: [expectation], timeout: 0.5)
    }

    func testUpdateCollection() {
        // given
        let expectation = expectation(description: "Success")
        let collectionService = CollectionService()
        collectionService.networkProvider = collectionNetworkService
        collectionService.cacheProvider = collectionCacheService

        let toUpdate = collections.first!
        collections[0] = Collection(id: toUpdate.id, title: "New title")

        // when
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            // then
            collectionService.updateCollection(toUpdate) { (_ inner: () throws -> Collection) in
                do {
                    let collection = try inner()

                    XCTAssertNotNil(collection)
                    XCTAssertEqual(collection, toUpdate)

                    XCTAssertNotNil(self.collectionNetworkService.collections)
                    XCTAssertEqual(self.collections, self.collectionNetworkService.collections)

                    XCTAssertNotNil(self.collectionCacheService.collections)
                    XCTAssertEqual(self.collections, self.collectionCacheService.collections)
                } catch {
                    XCTFail("Inner thrown unexpected error")
                }

            }
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 0.5)
    }

    func testFetchCollections() {
        // given
        let expectation = expectation(description: "Success")
        let collectionService = CollectionService()
        collectionService.networkProvider = collectionNetworkService
        collectionService.cacheProvider = collectionCacheService

        // when
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            // then
            collectionService.getCollections() { (_ inner: () throws -> [Collection]?) in
                do {
                    let collections = try inner()

                    XCTAssertNotNil(collections)
                    XCTAssertEqual(collections, self.collections)
                } catch {
                    XCTFail("Inner thrown unexpected error")
                }
            }

            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 0.5)
    }

    // MARK: Mockups
    final class СollectionNetworkServiceMockup: CollectionNetworkProvider {

        var collections: [Collection]!

        func storeCollection(_ collection: Remembrant.Collection, completion: @escaping (Result<Remembrant.Collection?, Error>) -> Void) {
            collections.append(collection)
            completion(.success(collection))
        }

        func getCollections(completion: @escaping (Result<[Remembrant.Collection]?, Error>) -> Void) {
            completion(.success(collections))
        }

        func updateCollection(_ collection: Remembrant.Collection, completion: @escaping (Result<Remembrant.Collection?, Error>) -> Void) {
            let index = collections.firstIndex(of: collection)
            collections[index!] = collection
            completion(.success(collection))
        }

        func deleteCollection(_ collection: Remembrant.Collection, completion: @escaping (Result<Remembrant.EmptyResponse?, Error>) -> Void) {
            collections.remove(at: collections.firstIndex(of: collection)!)
            completion(.success(EmptyResponse()))
        }
    }

    final class CollectionCacheServiceMockup: CollectionCacheProvider {
        var collections: [Collection]!

        func fetchCollections(completion: @escaping (Result<[Remembrant.Collection]?, Error>) -> Void) {
            completion(.success(collections))
        }

        func storeCollections(_ collections: [Remembrant.Collection]) {

        }

        func storeCollection(_ collection: Remembrant.Collection) {
            collections.append(collection)
        }

        func deleteCollection(_ collection: Remembrant.Collection) {
            collections.remove(at: collections.firstIndex(of: collection)!)
        }

        func updateCollection(_ collection: Remembrant.Collection) {
            let index = collections.firstIndex(of: collection)
            collections[index!] = collection
        }

    }
}
