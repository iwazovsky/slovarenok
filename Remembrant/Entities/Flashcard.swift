//
//  CollectionItem.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 14.12.2022.
//

import Foundation

struct Flashcard: Codable {
    let id: Int

    let title: String
    let content: String
}

extension Flashcard: Equatable {
    static func == (lhs: Flashcard, rhs: Flashcard) -> Bool {
        return lhs.id == rhs.id
    }
}
