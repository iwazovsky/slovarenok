//
//  Collectino.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 14.12.2022.
//

import Foundation

struct Collection: Codable {
    let id: Int
    let title: String
    let flashcards: [Flashcard]

    init(id: Int, title: String, flashcards: [Flashcard] = []) {
        self.id = id
        self.title = title
        self.flashcards = flashcards
    }
}

extension Collection: Equatable {
    static func == (lhs: Collection, rhs: Collection) -> Bool {
        lhs.id == rhs.id
    }
}
