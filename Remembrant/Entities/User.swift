//
//  User.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 22.11.2022.
//

import Foundation

struct User: Codable {
    var id: UUID = UUID()

    var email: String?

}
