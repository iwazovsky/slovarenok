//
//  CoreDataManager.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 08.01.2023.
//

import CoreData

enum DatabaseError: Error {
    case error
}

final class CoreDataManager {
    var modelName: String

    private lazy var mainContext: NSManagedObjectContext = {
        return self.persistentContainer.viewContext
    }()

    private lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: self.modelName)
        container.loadPersistentStores { _, error in
            if let error = error {
                fatalError("Unable to load persisent store \(error)")
            }
        }
        return container
    }()

    init(modelName: String) {
        self.modelName = modelName
    }

    func fetch<T: NSManagedObject>(predicate: NSPredicate?, sortBy: NSSortDescriptor? = nil, completion: @escaping(Result<[T], DatabaseError>) -> Void) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: T.self))
        fetchRequest.predicate = predicate

        if let sortBy = sortBy {
            fetchRequest.sortDescriptors = [sortBy]
        }

        guard let result = try? mainContext.fetch(fetchRequest) else {
            completion(.failure(.error))
            return
        }

        guard let models = result as? [T] else {
            return
        }

        completion(.success(models))
    }

    func fetchOne<T: NSManagedObject>(predicate: NSPredicate, completion: @escaping(Result<T, DatabaseError>) -> Void) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: T.self))
        fetchRequest.predicate = predicate

        guard let result = try? mainContext.fetch(fetchRequest) else {
            completion(.failure(.error))
            return
        }
        guard let model = result.first as? T else {
            completion(.failure(.error))
            return
        }
        completion(.success(model))
    }

    func store<T: NSManagedObject>(_ model: T.Type, values: [String: Any], completion: @escaping(Result<T, DatabaseError>) -> Void) {
        let entityName = String(describing: model.self)
        guard let entityDescription = NSEntityDescription.entity(forEntityName: entityName, in: mainContext) else {
            completion(.failure(.error))
            return
        }

        let entity = NSManagedObject(entity: entityDescription, insertInto: self.mainContext)
        entity.setValuesForKeys(values)

        guard let object = entity as? T else {
            completion(.failure(.error))
            return
        }

        guard mainContext.hasChanges else {
            completion(.failure(.error))
            return
        }

        do {
            try self.mainContext.save()
            completion(.success(object))
        } catch {
            completion(.failure(.error))
        }
    }

    func delete<T: NSManagedObject>(_ model: T) {
        self.mainContext.delete(model)
    }

    func update<T: NSManagedObject>(_ model: T, values: [String: Any], completion: @escaping(Result<T, DatabaseError>) -> Void) {
        model.setValuesForKeys(values)

        do {
            try mainContext.save()
            completion(.success(model))
        } catch {
            completion(.failure(.error))
        }
    }
}
