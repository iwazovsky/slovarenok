//
//  CollectionCacheService.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 08.01.2023.
//

import CoreData

protocol CollectionCacheProvider {
    func fetchCollections(completion: @escaping(Result<[Collection]?, Error>) -> Void)
    func storeCollections(_ collections: [Collection])
    func storeCollection(_ collection: Collection)
    func deleteCollection(_ collection: Collection)
    func updateCollection(_ collection: Collection)
}

enum CacheError: Error {
    case error
}

final class CollectionCacheService: CollectionCacheProvider {

    var coreDataManager: CoreDataManager!

    func fetchCollections(completion: @escaping (Result<[Collection]?, Error>) -> Void) {
        let sort = NSSortDescriptor(key: #keyPath(CollectionModel.id), ascending: false)

        coreDataManager.fetch(predicate: nil, sortBy: sort) { (result: Result<[CollectionModel], DatabaseError>) in
            switch result {
            case .success(let models):
                let collections = models.map({ model in

                    var flashcards: [Flashcard] = []
                    if let flashcardsModels = model.flashcards?.allObjects as? [FlashcardModel], !flashcardsModels.isEmpty {
                        flashcards = flashcardsModels.map { model in
                            Flashcard(id: Int(model.id), title: model.title ?? "", content: model.content ?? "")
                        }
                    }
                    return Collection(id: Int(model.id), title: model.title ?? "", flashcards: flashcards)
                })
                completion(Result.success(collections))
            case .failure(let error):
                print(error)
                completion(Result.failure(error))
            }
        }
    }

    func fetchCollection(_ collection: Collection, completion: @escaping(CollectionModel?) -> Void) {
        coreDataManager.fetchOne(predicate: NSPredicate(format: "id == %@", NSNumber(value: collection.id))) { (result: Result<CollectionModel, DatabaseError>) in
            switch result {
            case .success(let model):
                completion(model)
            case .failure(let error):
                print(error)
                // TODO: Handle error (log)
                completion(nil)
            }
        }
    }

    func storeCollection(_ collection: Collection) {
        var values: [String: Any] = [
            "id": Int32(collection.id),
            "title": collection.title
        ]

        coreDataManager.store(CollectionModel.self, values: values) { (result: Result<CollectionModel, DatabaseError>) in
            // TODO: Handle error on collection save into cache

            switch result {
            case .success(let collectionModel):
                for flashcard in collection.flashcards {
                    values = [
                        "id": Int32(flashcard.id),
                        "title": flashcard.title,
                        "content": flashcard.content,
                        "collection": collectionModel
                    ]

                    self.coreDataManager.store(FlashcardModel.self, values: values) { (result: Result<FlashcardModel, DatabaseError>) in
                    }
                }

                return
            case .failure(let error):
                print(error)
                return
            }
        }
    }

    func storeCollections(_ collections: [Collection]) {
        for collection in collections {
            self.storeCollection(collection)
        }
    }

    func updateCollection(_ collection: Collection) {
        fetchCollection(collection) { collectionModel in
            if let model = collectionModel {
                let values: [String: Any] = [
                    "id": Int32(collection.id),
                    "title": collection.title
                ]
                self.coreDataManager.update(model, values: values) { (_: Result<CollectionModel, DatabaseError>) in

                }
            }
        }
    }

    func deleteCollection(_ collection: Collection) {
        coreDataManager.fetchOne(predicate: NSPredicate(format: "id == %@", NSNumber(value: collection.id))) { (result: Result<CollectionModel, DatabaseError>) in
            switch result {
            case .success(let model):
                self.coreDataManager.delete(model)
                return
            case .failure(let error):
                print(error)
                return
            }
        }
    }

}
