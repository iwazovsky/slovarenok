//
//  FlashcardCacheService.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 08.01.2023.
//

import Foundation

protocol FlashcardCacheProvider {
    func fetchFlashcards(collection: Collection, completion: @escaping(Result<[Flashcard], Error>) -> Void)
    func storeFlashcard(_ flashcard: Flashcard, collection: Collection)
    func updateFlashcard(_ flashcard: Flashcard)
    func deleteFlashcard(_ flashcard: Flashcard)
}

final class FlashcardCacheService: FlashcardCacheProvider {

    var coreDataManager: CoreDataManager!

    func fetchFlashcards(collection: Collection, completion: @escaping(Result<[Flashcard], Error>) -> Void) {
        let predicate = NSPredicate(format: "collection.id == %@", NSNumber(value: collection.id))
        let sort = NSSortDescriptor(key: #keyPath(FlashcardModel.id), ascending: false)

        coreDataManager.fetch(predicate: predicate, sortBy: sort) { (result: Result<[FlashcardModel], DatabaseError>) in
            switch result {
            case .success(let models):
                let flashcards = models.map { model in
                    Flashcard(id: Int(model.id), title: model.title ?? "", content: model.content ?? "")
                }

                completion(.success(flashcards))
                return
            case .failure(let error):
                print(error.localizedDescription)
                completion(.failure(DatabaseError.error))
                return
            }
        }
    }

    func storeFlashcard(_ flashcard: Flashcard, collection: Collection) {
        coreDataManager.fetchOne(predicate: NSPredicate(format: "id = %@", NSNumber(value: collection.id))) { (result: Result<CollectionModel, DatabaseError>) in

            switch result {
            case .success(let model):
                let values: [String: Any] = [
                    "id": Int32(flashcard.id),
                    "title": flashcard.title,
                    "content": flashcard.content,
                    "collection": model
                ]
                self.coreDataManager.store(FlashcardModel.self, values: values) { _ in }
            case .failure(let error):
                print(error)
            }

        }
    }

    func updateFlashcard(_ flashcard: Flashcard) {
        coreDataManager.fetchOne(predicate: NSPredicate(format: "id = %@", NSNumber(value: flashcard.id))) { (result: Result<FlashcardModel, DatabaseError>) in
            switch result {
            case .success(let model):
                let values: [String: Any] = [
                    "title": flashcard.title,
                    "content": flashcard.content
                ]
                self.coreDataManager.update(model, values: values) { _ in }
                return
            case .failure(let error):
                print(error)
                return
            }
        }
    }

    func deleteFlashcard(_ flashcard: Flashcard) {
        coreDataManager.fetchOne(predicate: NSPredicate(format: "id == %@", NSNumber(value: flashcard.id))) { (result: Result<FlashcardModel, DatabaseError>) in

            switch result {
            case .success(let model):
                self.coreDataManager.delete(model)
                return
            case .failure:
                return
            }

        }
    }
}
