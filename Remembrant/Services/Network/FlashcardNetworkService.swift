//
//  FlashcardNetworkService.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 08.01.2023.
//

import Foundation

protocol FlashcardNetworkProvider {
    func storeFlashcard(collection: Collection, flashcard: Flashcard, completion: @escaping(Result<Flashcard?, Error>) -> Void)
    func updateFlashcard(collection: Collection, flashcard: Flashcard, completion: @escaping(Result<Flashcard?, Error>) -> Void)
    func deleteFlashcard(collection: Collection, flashcard: Flashcard, completion: @escaping(Result<EmptyResponse?, Error>) -> Void)
}

// MARK: Service
final class FlashcardNetworkService: FlashcardNetworkProvider {

    // MARK: Dependencies
    var client: RemembrantApiClient!
    var jsonHelper: JsonHelper!

    // MARK: Methods
    func storeFlashcard(collection: Collection, flashcard: Flashcard, completion: @escaping (Result<Flashcard?, Error>) -> Void) {
        client.request(path: "collections/\(collection.id)/flashcards", method: .post, data: jsonHelper.encode(flashcard), completion: completion)
    }

    func updateFlashcard(collection: Collection, flashcard: Flashcard, completion: @escaping(Result<Flashcard?, Error>) -> Void) {
        client.request(path: "collections/\(collection.id)/flashcards/\(flashcard.id)", method: .put, data: jsonHelper.encode(flashcard), completion: completion)
    }

    func deleteFlashcard(collection: Collection, flashcard: Flashcard, completion: @escaping(Result<EmptyResponse?, Error>) -> Void) {
        client.request(path: "collections/\(collection.id)/flashcards/\(flashcard.id)", method: .delete, completion: completion)
    }
}
