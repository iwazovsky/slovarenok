//
//  RemembrantApiClient.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 27.12.2022.
//

import Foundation

enum HttpClientMethod: String {
    case get, post, put, delete

    var value: String {
        rawValue.uppercased()
    }
}

final class RemembrantApiClient {
    // MARK: Dependencies
    var jsonHelper: JsonHelper!

    private var clientCfg: ClientConfiguration = RemembrantClientConfiguration()
    private let urlSessionCfg = URLSessionConfiguration.default
    private var dataTask: URLSessionDataTask?

    func request<T: Decodable>(path: String, method: HttpClientMethod, parameters: [String: String] = [:], data: Data? = nil, completion: @escaping(Result<T?, Error>) -> Void) {
        let path = "\(clientCfg.baseUrl)\(path)"
        guard let url = URL(string: path) else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = method.value

        for header in clientCfg.headers {
            request.setValue(header.value, forHTTPHeaderField: header.key)
        }

        if let data = data {
            request.httpBody = data
        }

        let session = URLSession(configuration: urlSessionCfg)

        dataTask = session.dataTask(with: request) { (data, response, error) in

            if let error = error {
                completion(.failure(error))
                return
            }

            if let response = response as? HTTPURLResponse {
                if response.statusCode < 200 && response.statusCode >= 400 {
//                    completion(.failure)
                    return
                }
            }

            if let data = data {
                var content: T?

                if !data.isEmpty {
                    content = self.jsonHelper.decode(data: data, T.self)
                }
                completion(.success(content))
            }
        }

        dataTask?.resume()
    }
}
