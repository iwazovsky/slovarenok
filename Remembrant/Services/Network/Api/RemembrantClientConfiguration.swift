//
//  RemembrantClientConfiguration.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 27.12.2022.
//

import Foundation

protocol ClientConfiguration {
    var baseUrl: String { get }
    var headers: [String: String] { get }
}

struct RemembrantClientConfiguration: ClientConfiguration {

    let baseUrl = "https://remembrant.eu/"

    let headers = ["Content-Type": "application/json"]
}
