//
//  CreateCollectionResponse.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 28.12.2022.
//

import Foundation

struct CreateCollectionResponse: Decodable {
    let id: Int
}
