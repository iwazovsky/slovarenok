//
//  JsonHelper.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 27.12.2022.
//

import Foundation

final class JsonHelper {

    private lazy var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        return decoder
    }()
    private lazy var encoder: JSONEncoder = {
        let encoder = JSONEncoder()
        return encoder
    }()

    func decode<T: Decodable>(data: Data, _ type: T.Type) -> T? {
        var item: T?

        do {
            item = try decoder.decode(type, from: data)
        } catch {
            // TODO: Handle error on JSONHelper.decode
            print(error)
        }

        return item
    }

    func encode<T: Encodable>(_ instance: T, encoding: String.Encoding = .utf8) -> Data? {
        do {
            return try encoder.encode(instance)
        } catch {
            // TODO: Handle error on JSONHelper.encode
            print(error)
            return nil
        }

    }
}
