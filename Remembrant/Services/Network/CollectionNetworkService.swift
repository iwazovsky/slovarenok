//
//  CollectionNetworkService.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 08.01.2023.
//

import Foundation

protocol CollectionNetworkProvider {
    func storeCollection(_ collection: Collection, completion: @escaping(Result<Collection?, Error>) -> Void)
    func getCollections(completion: @escaping(Result<[Collection]?, Error>) -> Void)
    func updateCollection(_ collection: Collection, completion: @escaping(Result<Collection?, Error>) -> Void)
    func deleteCollection(_ collection: Collection, completion: @escaping(Result<EmptyResponse?, Error>) -> Void)
}

// MARK: Service
final class CollectionNetworkService: CollectionNetworkProvider {

    // MARK: Dependencies
    var jsonHelper: JsonHelper!
    var client: RemembrantApiClient!

    // MARK: Methods
    func getCollections(completion: @escaping(Result<[Collection]?, Error>) -> Void) {
        client.request(path: "collections", method: .get, completion: completion)
    }

    func storeCollection(_ collection: Collection, completion: @escaping(Result<Collection?, Error>) -> Void) {
        client.request(path: "collections", method: .post, data: jsonHelper.encode(collection), completion: completion)
    }

    func deleteCollection(_ collection: Collection, completion: @escaping(Result<EmptyResponse?, Error>) -> Void) {
        client.request(path: "collections/\(collection.id)", method: .delete, completion: completion)
    }

    func updateCollection(_ collection: Collection, completion: @escaping (Result<Collection?, Error>) -> Void) {
        client.request(path: "collections/\(collection.id)", method: .put, data: jsonHelper.encode(collection), completion: completion)
    }
}
