//
//  CollectionService.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import Foundation

typealias GetCollectionThrowableCallback = () throws -> [Collection]?
typealias CreateCollectionThrowableCallback = () throws -> Collection
typealias DeleteCollectionThrowableCallback = () throws -> Void
typealias UpdateCollectionThrowableCallback = () throws -> Collection

protocol CollectionProvider {
    func getCollections(completion: @escaping(_ inner: GetCollectionThrowableCallback) -> Void)
    func updateCollection(_ collection: Collection, completion: @escaping(_ inner: UpdateCollectionThrowableCallback) -> Void)
    func storeCollection(_ collection: Collection, completion: @escaping(_ inner: CreateCollectionThrowableCallback) -> Void)
    func deleteCollection(_ collection: Collection, completion: @escaping(_ inner: DeleteCollectionThrowableCallback) -> Void)
}

final class CollectionService: CollectionProvider {

    var cacheProvider: CollectionCacheProvider!
    var networkProvider: CollectionNetworkProvider!

    func getCollections(completion: @escaping(_ inner: GetCollectionThrowableCallback) -> Void) {
        cacheProvider.fetchCollections { (result: Result<[Collection]?, Error>) in
            switch result {
            case .success(let collections):
                if !(collections ?? []).isEmpty {
                    completion({collections})
                    return
                }

                self.networkProvider.getCollections { (result: Result<[Collection]?, Error>) in
                    switch result {
                    case .success(let collections):
                        guard let collections = collections else {
                            // TODO: Throw and handle an error
                            // completion({ throw })
                            return
                        }

                        self.cacheProvider.storeCollections(collections)
                        completion({ collections })

                    case .failure(let error):
                        // TODO: Throw and handle an error
                        completion({ throw error })
                    }
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }

    func storeCollection(_ collection: Collection, completion: @escaping(_ inner: CreateCollectionThrowableCallback) -> Void) {
        networkProvider.storeCollection(collection) { (result: Result<Collection?, Error>) in
            switch result {
            case .success(let collection):
                guard let collection = collection else {
                    // TODO: Error handling
//                    completion({ throw })
                    return
                }
                self.cacheProvider.storeCollection(collection)
                completion({ collection })
            case .failure(let error):
                completion({ throw error })
            }
        }
    }

    func deleteCollection(_ collection: Collection, completion: @escaping(_ inner: DeleteCollectionThrowableCallback) -> Void) {

        networkProvider.deleteCollection(collection) { (result: Result<EmptyResponse?, Error>) in
            switch result {
            case .success(let emptyResponse):
                self.cacheProvider.deleteCollection(collection)
                completion({})
            case .failure(let error):
                completion({ throw error })
            }
        }
    }

    func updateCollection(_ collection: Collection, completion: @escaping(_ inner: UpdateCollectionThrowableCallback) -> Void) {
        networkProvider.updateCollection(collection) { (result: Result<Collection?, Error>) in
            switch result {
            case .success(let collection):
                guard let collection = collection else {
                    // TODO: Error handling
                    // completion({ throw })
                    return
                }
                self.cacheProvider.updateCollection(collection)
                completion({ collection })
            case .failure(let error):
                completion({ throw error })
            }
        }

    }

}
