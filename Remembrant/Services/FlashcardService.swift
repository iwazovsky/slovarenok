//
//  FlashcardService.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 02.01.2023.
//

import Foundation

typealias GetFlashcardsThrowableCallback = () throws -> [Flashcard]
typealias CreateFlashcardThrowableCallback = () throws -> Flashcard
typealias UpdateFlashcardThrowableCallback = () throws -> Flashcard
typealias DeleteFlashcardThrowableCallback = () throws -> Void

protocol FlashcardProvider {
    func fetchFlashcards(collection: Collection, completion: @escaping(_ inner: GetFlashcardsThrowableCallback) -> Void)
    func storeFlashcard(collection: Collection, flashcard: Flashcard, completion: @escaping(_ inner: CreateFlashcardThrowableCallback) -> Void)
    func updateFlashcard(collection: Collection, flashcard: Flashcard, completion: @escaping(_ inner: UpdateFlashcardThrowableCallback) -> Void)
    func deleteFlashcard(collection: Collection, flashcard: Flashcard, completion: @escaping (_ inner: DeleteFlashcardThrowableCallback) -> Void)
}

final class FlashcardService: FlashcardProvider {

    var jsonHelper: JsonHelper!
    var networkProvider: FlashcardNetworkProvider!
    var cacheProvider: FlashcardCacheProvider!

    func fetchFlashcards(collection: Collection, completion: @escaping (() throws -> [Flashcard]) -> Void) {
        cacheProvider.fetchFlashcards(collection: collection) { (result: Result<[Flashcard], Error>) in
            switch result {
            case .success(let flashcards):
                completion({ flashcards })
                return
            case .failure(let error):
                completion({ throw error })
                return
            }
        }
    }

    func storeFlashcard(collection: Collection, flashcard: Flashcard, completion: @escaping (CreateFlashcardThrowableCallback) -> Void) {
        networkProvider.storeFlashcard(collection: collection, flashcard: flashcard) { (result: Result<Flashcard?, Error>) in
            switch result {
            case .success(let flashcard):
                guard let flashcard = flashcard else {
                    // TODO: Handle error
                    return
                }
                self.cacheProvider.storeFlashcard(flashcard, collection: collection)
                completion({ flashcard })
            case .failure(let error):
                completion({ throw error })
            }
        }
    }

    func updateFlashcard(collection: Collection, flashcard: Flashcard, completion: @escaping (() throws -> Flashcard) -> Void) {
        networkProvider.updateFlashcard(collection: collection, flashcard: flashcard) { (result: Result<Flashcard?, Error>) in
            switch result {
            case .success(let flashcard):
                guard let flashcard = flashcard else {
                    // TODO: Handle error
                    return
                }

                self.cacheProvider.updateFlashcard(flashcard)
                completion({ flashcard })
            case .failure(let error):
                completion({ throw error })
            }
        }
    }

    func deleteFlashcard(collection: Collection, flashcard: Flashcard, completion: @escaping (DeleteFlashcardThrowableCallback) -> Void) {
        networkProvider.deleteFlashcard(collection: collection, flashcard: flashcard) { (result: Result<EmptyResponse?, Error>) in
            switch result {
            case .success(let empty):
                self.cacheProvider.deleteFlashcard(flashcard)

                completion({})
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}
