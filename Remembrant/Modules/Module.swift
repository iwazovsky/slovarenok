//
//  Module.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 21.11.2022.
//

import UIKit

protocol ModuleInput {

}

protocol ModuleOutput {

}

protocol Module {

    var moduleOutput: ModuleOutput? { get }

    func create(for navigationController: UINavigationController?, moduleInput: ModuleInput?) -> UIViewController

}
