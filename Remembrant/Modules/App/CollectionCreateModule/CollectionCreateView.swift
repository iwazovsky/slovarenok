//
//  CollectionCreateView.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import UIKit

protocol CollectionCreateViewInput: AnyObject {
    func collectionCreated()
}

protocol CollectionCreateViewOutput {
    func createCollection(title: String?)
    func cancelCreate()
}

// MARK: View
final class CollectionCreateView: UIViewController {

    static let localizationPrefix = "collectionCreate"

    // MARK: View elements
    private lazy var navbar: UINavigationBar = {
        let navbar = UINavigationBar()
        navbar.backgroundColor = .defaultModalBgColor
        navbar.delegate = self

        let navigationItem = UINavigationItem(title: NSLocalizedString("\(CollectionCreateView.localizationPrefix)_title", comment: "Title for create collection modal"))
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(onCancelTap))
        navigationItem.rightBarButtonItem = createBarButtonItem
        navbar.items = [navigationItem]

        navbar.translatesAutoresizingMaskIntoConstraints = false
        return navbar
    }()

    private lazy var createBarButtonItem: UIBarButtonItem = {
        return UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(onCreateButtonTap))
    }()

    private lazy var loadingIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        return indicator
    }()

    private lazy var loadingBarButtonItem: UIBarButtonItem = {
        let barButton = UIBarButtonItem(customView: loadingIndicator)
        return barButton
    }()

    private lazy var titleInputLabel: UILabel = {
        let label = UILabel()
        label.text = NSLocalizedString("\(CollectionCreateView.localizationPrefix)_titleField_placeholder", comment: "Placeholder for collection key input field")
        label.textColor = .secondaryTextColor
        label.font = .boldSystemFont(ofSize: UILabel.secondarySize)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var titleInputField: CustomTextField = {
        let field = CustomTextField()
        field.font = .systemFont(ofSize: UILabel.bodySize)
        field.textColor = .primaryTextColor
        field.backgroundColor = .inputBgColor
        field.layer.cornerRadius = 16
        field.translatesAutoresizingMaskIntoConstraints = false
        return field
    }()

    var output: CollectionCreateViewOutput!

    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .defaultModalBgColor

        initViews()
        initConstraints()

        titleInputField.becomeFirstResponder()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        resignFirstResponder()
    }

    // MARK: View inits
    private func initViews() {
        view.addSubview(navbar)
        view.addSubview(titleInputLabel)
        view.addSubview(titleInputField)
    }

    private func initConstraints() {
        let safeArea = view.safeAreaLayoutGuide

        NSLayoutConstraint.activate([
            navbar.topAnchor.constraint(equalTo: safeArea.topAnchor),
            navbar.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            navbar.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
            navbar.heightAnchor.constraint(equalToConstant: 76),

            titleInputLabel.topAnchor.constraint(equalTo: navbar.bottomAnchor, constant: 16),
            titleInputLabel.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 16),

            titleInputField.topAnchor.constraint(equalTo: titleInputLabel.bottomAnchor, constant: 8),
            titleInputField.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 16),
            titleInputField.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: -16)
        ])
    }

}

// MARK: View events
extension CollectionCreateView {
    @objc private func onCreateButtonTap() {
        guard let item = navbar.items?.first else {
            return
        }
        loadingIndicator.startAnimating()
        item.rightBarButtonItem = loadingBarButtonItem
        output.createCollection(title: titleInputField.text)
    }

    @objc private func onCancelTap() {

        output.cancelCreate()
    }
}

extension CollectionCreateView: UINavigationBarDelegate {
}

// MARK: View input
extension CollectionCreateView: CollectionCreateViewInput {
    func collectionCreated() {
        loadingIndicator.stopAnimating()

        guard let item = navbar.items?.first else {
            return
        }
        item.leftBarButtonItem = createBarButtonItem
    }
}
