//
//  CollectionCreateModule.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import UIKit

final class CollectionCreateModule: Module {

    let moduleOutput: ModuleOutput? = nil

    func create(for navigationController: UINavigationController?, moduleInput: ModuleInput?) -> UIViewController {
        let viewController = CollectionCreateView()
        let configurator = CollectionCreateConfigurator()
        configurator.configure(viewController: viewController, navigationController: navigationController, moduleInput: moduleInput)
        return viewController
    }
}
