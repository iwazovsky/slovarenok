//
//  CollectionCreateModuleInput.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 28.12.2022.
//

import Foundation

typealias CollectionCreateDismissCallback = (_ collection: Collection) -> Void

final class CollectionCreateModuleInput: ModuleInput {

    let onDismiss: CollectionCreateDismissCallback

    init(onDismiss: @escaping CollectionCreateDismissCallback) {
        self.onDismiss = onDismiss
    }
}
