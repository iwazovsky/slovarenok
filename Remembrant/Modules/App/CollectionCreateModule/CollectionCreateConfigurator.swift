//
//  CollectionCreateConfigurator.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import UIKit

final class CollectionCreateConfigurator: Configurator {
    func configure(viewController: UIViewController, navigationController: UINavigationController?, moduleInput: ModuleInput?, moduleOutput: ModuleOutput?) {
        guard let viewController = viewController as? CollectionCreateView else {
            fatalError("Wrong view controller passed to configurator")
        }

        guard let moduleInput = moduleInput as? CollectionCreateModuleInput else {
            fatalError("Wrong module input passed to configurator")
        }

        let router = CollectionCreateRouter()
        router.navigationController = navigationController
        router.onDismiss = moduleInput.onDismiss

        let interactor = CollectionCreateInteractor()

        let collectionProviderKey = String(describing: CollectionService.self)
        interactor.collectionProvider = DIContainer.shared.resolve(collectionProviderKey)

        let presenter = CollectionCreatePresenter()
        presenter.router = router
        presenter.interactor = interactor
        presenter.view = viewController

        interactor.output = presenter

        viewController.output = presenter
    }
}
