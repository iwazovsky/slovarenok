//
//  CollectionCreateInteractor.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import Foundation

protocol CollectionCreateInteractorInput {
    func createCollection(title: String)
}

protocol CollectionCreateInteractorOutput: AnyObject {
    func collectionCreated(collection: Collection)
}

// MARK: Interactor
final class CollectionCreateInteractor {

    weak var output: CollectionCreateInteractorOutput!
    var collectionProvider: CollectionProvider!

}

// MARK: Interactor input
extension CollectionCreateInteractor: CollectionCreateInteractorInput {
    func createCollection(title: String) {
        // do network request to create collection
        let collection = Collection(id: 0, title: title, flashcards: [])
        collectionProvider.storeCollection(collection) { (inner: CreateCollectionThrowableCallback) in
            do {
                let collection = try inner()
                self.output.collectionCreated(collection: collection)
            } catch {
                print(error)
            }

        }
    }
}
