//
//  CollectionCreateRouter.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import UIKit

protocol CollectionCreateRouterInput {
    func closeModal(collection: Collection)
    func cancel()
}

final class CollectionCreateRouter: CollectionCreateRouterInput {

    var navigationController: UINavigationController!
    var onDismiss: CollectionCreateDismissCallback!

    func closeModal(collection: Collection) {
        navigationController.dismiss(animated: true)
        onDismiss(collection)
    }

    func cancel() {
        navigationController.dismiss(animated: true)
    }
}
