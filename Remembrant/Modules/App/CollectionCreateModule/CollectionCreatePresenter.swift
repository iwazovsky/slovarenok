//
//  CollectionCreatePresenter.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import Foundation

// MARK: Presenter
final class CollectionCreatePresenter {
    weak var view: CollectionCreateViewInput!
    var interactor: CollectionCreateInteractorInput!
    var router: CollectionCreateRouterInput!
}

// MARK: Presenter output
extension CollectionCreatePresenter: CollectionCreateViewOutput {
    func createCollection(title: String?) {
        guard let title = title else { return }

        DispatchQueue.global().async {
            self.interactor.createCollection(title: title)
        }
    }

    func cancelCreate() {
        router.cancel()
    }
}

extension CollectionCreatePresenter: CollectionCreateInteractorOutput {
    func collectionCreated(collection: Collection) {
        DispatchQueue.main.sync {
            // TODO: On presenter output receive
            view.collectionCreated()

            router.closeModal(collection: collection)
        }
    }
}
