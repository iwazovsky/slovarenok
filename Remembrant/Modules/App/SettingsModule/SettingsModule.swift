//
//  SettingsModule.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 26.11.2022.
//

import UIKit

final class SettingsModule: Module {

    let moduleOutput: ModuleOutput? = nil

    func create(for navigationController: UINavigationController?, moduleInput: ModuleInput?) -> UIViewController {
        let viewController = SettingsView()
        let configurator = SettingsConfigurator()
        configurator.configure(viewController: viewController, navigationController: navigationController, moduleInput: moduleInput)
        return viewController
    }
}
