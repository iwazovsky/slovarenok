//
//  SettingsPresenter.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 26.11.2022.
//

import Foundation

final class SettingsPresenter {
    weak var view: SettingsViewInput!
    var interactor: SettingsInteractorInput!
    var router: SettingsRouterInput!
}

extension SettingsPresenter: SettingsViewOutput {

}
