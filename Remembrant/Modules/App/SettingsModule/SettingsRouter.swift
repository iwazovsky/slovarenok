//
//  SettingsRouter.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 26.11.2022.
//

import UIKit

protocol SettingsRouterInput { }

final class SettingsRouter: SettingsRouterInput {
    var navigationController: UINavigationController!
}
