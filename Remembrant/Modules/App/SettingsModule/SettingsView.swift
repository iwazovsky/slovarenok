//
//  SettingsView.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 26.11.2022.
//

import UIKit

protocol SettingsViewInput: AnyObject {

}

protocol SettingsViewOutput {

}

final class SettingsView: UIViewController {

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = NSLocalizedString("settings_title", comment: "Title for settings tab")
        label.font = .systemFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    var output: SettingsViewOutput!

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .defaultBgColor

        initViews()
        initConstraints()
    }

    private func initViews() {
        view.addSubview(titleLabel)
    }

    private func initConstraints() {
        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: 10),
            titleLabel.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 10)
        ])
    }
}

extension SettingsView: SettingsViewInput {

}
