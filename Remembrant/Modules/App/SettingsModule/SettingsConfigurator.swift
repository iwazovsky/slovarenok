//
//  SettingsConfigurator.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 26.11.2022.
//

import UIKit

final class SettingsConfigurator: Configurator {
    func configure(viewController: UIViewController, navigationController: UINavigationController?, moduleInput: ModuleInput?, moduleOutput: ModuleOutput?) {
        guard let viewController = viewController as? SettingsView else { return }

        let router = SettingsRouter()
        router.navigationController = navigationController

        let interactor = SettingsInteractor()

        let presenter = SettingsPresenter()
        presenter.router = router
        presenter.interactor = interactor
        presenter.view = viewController

        viewController.output = presenter
    }
}
