//
//  FlashcardEditPresenter.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 03.01.2023.
//

import Foundation

// MARK: Presenter
final class FlashcardEditPresenter {

    // MARK: Dependencies
    weak var view: FlashcardEditViewInput!
    var router: FlashcardEditRouterInput!
    var interactor: FlashcardEditInteractorInput!

    var collection: Collection!
    var flashcard: Flashcard!
}

// MARK: View output
extension FlashcardEditPresenter: FlashcardEditViewOutput {
    func getFlashcard() -> Flashcard {
        return flashcard
    }

    func updateFlashcard(title: String?, content: String?) {
        DispatchQueue.main.async {
            // TODO: Validation fill
            guard let title = title else {
                return
            }
            guard let content = content else {
                return
            }

            self.interactor.updateFlashcard(collection: self.collection, id: self.flashcard.id, title: title, content: content)
        }
    }

    func cancelEdit() {
        router.cancelEdit()
    }

}

// MARK: Interactor output
extension FlashcardEditPresenter: FlashcardEditInteractorOutput {
    func flashcardUpdated(flashcard: Flashcard) {
        DispatchQueue.main.sync {
            view.flashcardUpdated()
            router.closeModal(flashcard: flashcard)
        }
    }
}
