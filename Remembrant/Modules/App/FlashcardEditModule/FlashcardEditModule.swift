//
//  FlashcardEditModule.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 03.01.2023.
//

import UIKit

final class FlashcardEditModule: Module {

    let moduleOutput: ModuleOutput? = nil

    func create(for navigationController: UINavigationController?, moduleInput: ModuleInput?) -> UIViewController {
        let viewController = FlashcardEditView()
        let configurator = FlashcardEditConfigurator()
        configurator.configure(viewController: viewController, navigationController: navigationController, moduleInput: moduleInput)
        return viewController
    }
}
