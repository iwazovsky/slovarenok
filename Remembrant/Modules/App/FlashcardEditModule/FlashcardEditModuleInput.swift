//
//  FlashcardEditModuleInput.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 03.01.2023.
//

import Foundation

typealias FlashcardEditDismissCallback = (Flashcard) -> Void

final class FlashcardEditModuleInput: ModuleInput {
    var collection: Collection
    var flashcard: Flashcard
    var onDismiss: FlashcardEditDismissCallback

    init(collection: Collection, flashcard: Flashcard, onDismiss: @escaping FlashcardEditDismissCallback) {
        self.collection = collection
        self.flashcard = flashcard
        self.onDismiss = onDismiss
    }
}
