//
//  FlashcardEditConfigurator.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 03.01.2023.
//

import UIKit

final class FlashcardEditConfigurator: Configurator {
    func configure(viewController: UIViewController, navigationController: UINavigationController?, moduleInput: ModuleInput?, moduleOutput: ModuleOutput?) {
        guard let viewController = viewController as? FlashcardEditView else {
            fatalError("Wrong view passed into FlashcardEditConfigurator")
        }

        guard let moduleInput = moduleInput as? FlashcardEditModuleInput else {
            fatalError("Wrong module input passed into FlashcardEditModuleInput")
        }

        let router = FlashcardEditRouter()
        router.navigationController = navigationController
        router.onDismiss = moduleInput.onDismiss

        let interactor = FlashcardEditInteractor()
        let flashcardProviderType = String(describing: FlashcardService.self)
        interactor.flashcardProvider = DIContainer.shared.resolve(flashcardProviderType)

        let presenter = FlashcardEditPresenter()
        presenter.view = viewController
        presenter.router = router
        presenter.interactor = interactor
        presenter.collection = moduleInput.collection
        presenter.flashcard = moduleInput.flashcard

        interactor.output = presenter

        viewController.output = presenter

    }
}
