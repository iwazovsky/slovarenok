//
//  FlashcardEditInteractor.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 03.01.2023.
//

import Foundation

protocol FlashcardEditInteractorInput {
    func updateFlashcard(collection: Collection, id: Int, title: String, content: String)
}

protocol FlashcardEditInteractorOutput: AnyObject {
    func flashcardUpdated(flashcard: Flashcard)
}

// MARK: Interactor
final class FlashcardEditInteractor: FlashcardEditInteractorInput {
    weak var output: FlashcardEditInteractorOutput!
    var flashcardProvider: FlashcardProvider!

    func updateFlashcard(collection: Collection, id: Int, title: String, content: String) {
        let flashcard = Flashcard(id: id, title: title, content: content)

        flashcardProvider.updateFlashcard(collection: collection, flashcard: flashcard) { (_ inner: UpdateFlashcardThrowableCallback) in
            do {
                let updatedFlashcard = try inner()
                self.output.flashcardUpdated(flashcard: updatedFlashcard)
            } catch {
                print(error)
            }
        }
    }
}
