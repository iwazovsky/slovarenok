//
//  FlashcardEditRouter.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 03.01.2023.
//

import UIKit

protocol FlashcardEditRouterInput {
    func closeModal(flashcard: Flashcard)
    func cancelEdit()
}

final class FlashcardEditRouter: FlashcardEditRouterInput {
    var navigationController: UINavigationController!
    var onDismiss: FlashcardEditDismissCallback!

    func closeModal(flashcard: Flashcard) {
        navigationController.dismiss(animated: true)
        onDismiss(flashcard)
    }

    func cancelEdit() {
        navigationController.dismiss(animated: true)
    }
}
