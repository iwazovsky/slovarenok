//
//  FlashcardEditView.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 03.01.2023.
//

import UIKit

protocol FlashcardEditViewInput: AnyObject {
    func flashcardUpdated()
}

protocol FlashcardEditViewOutput {
    func getFlashcard() -> Flashcard
    func updateFlashcard(title: String?, content: String?)
    func cancelEdit()
}

// MARK: View
final class FlashcardEditView: UIViewController {

    static let localizationPrefix = "flashcardEdit"

    // MARK: View Elements
    private lazy var navbar: UINavigationBar = {
        let navbar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 76))
        navbar.backgroundColor = .defaultModalBgColor
        navbar.delegate = self

        let navItem = UINavigationItem(title: NSLocalizedString("\(FlashcardEditView.localizationPrefix)_title", comment: "Title for flashcard edit"))
        navItem.rightBarButtonItem = editBarButtonItem
        navItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(onCancelTap))
        navbar.items = [navItem]

        navbar.translatesAutoresizingMaskIntoConstraints = false

        return navbar
    }()

    private lazy var editBarButtonItem: UIBarButtonItem = {
        return UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(onEditTap))
    }()

    private lazy var loadingButtonItem: UIBarButtonItem = {
        let barButton = UIBarButtonItem(customView: loadingIndicator)
        return barButton
    }()

    private lazy var loadingIndicator: UIActivityIndicatorView = {
        return UIActivityIndicatorView()
    }()

    private lazy var flashcardTitleLabel: UILabel = {
        let label = UILabel()
        label.text = NSLocalizedString("\(FlashcardEditView.localizationPrefix)_titleField_placeholder", comment: "Flashcard title input placeholder")
        label.textColor = .secondaryTextColor
        label.font = .boldSystemFont(ofSize: UILabel.secondarySize)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var flashcardTitleInput: CustomTextField = {
        let input = CustomTextField()
        input.font = .systemFont(ofSize: UILabel.bodySize)
        input.textColor = .primaryTextColor
        input.backgroundColor = .inputBgColor
        input.layer.cornerRadius = 16
        input.translatesAutoresizingMaskIntoConstraints = false
        return input
    }()

    private lazy var flashcardContentLabel: UILabel = {
        let label = UILabel()
        label.text = NSLocalizedString("\(FlashcardEditView.localizationPrefix)_valueField_placeholder", comment: "Flashcard value input placeholder")
        label.textColor = .secondaryTextColor
        label.font = .boldSystemFont(ofSize: UILabel.secondarySize)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var flashcardContentInput: CustomTextField = {
        let input = CustomTextField()
        input.font = .systemFont(ofSize: UILabel.bodySize)
        input.textColor = .primaryTextColor
        input.backgroundColor = .inputBgColor
        input.layer.cornerRadius = 16
        input.translatesAutoresizingMaskIntoConstraints = false
        input.translatesAutoresizingMaskIntoConstraints = false
        return input
    }()

    // MARK: Dependencies
    var output: FlashcardEditViewOutput!

    // MARK: Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .defaultModalBgColor

        initViews()
        initConstraints()
        initData()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        resignFirstResponder()
    }

    // MARK: Init methods
    private func initViews() {
        view.addSubview(navbar)
        view.addSubview(flashcardTitleLabel)
        view.addSubview(flashcardTitleInput)
        view.addSubview(flashcardContentLabel)
        view.addSubview(flashcardContentInput)

        flashcardTitleInput.becomeFirstResponder()
    }

    private func initConstraints() {
        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            navbar.topAnchor.constraint(equalTo: safeArea.topAnchor),
            navbar.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            navbar.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),

            flashcardTitleLabel.topAnchor.constraint(equalTo: navbar.bottomAnchor, constant: 16),
            flashcardTitleLabel.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 16),

            flashcardTitleInput.topAnchor.constraint(equalTo: flashcardTitleLabel.bottomAnchor, constant: 8),
            flashcardTitleInput.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 16),
            flashcardTitleInput.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: -16),

            flashcardContentLabel.topAnchor.constraint(equalTo: flashcardTitleInput.bottomAnchor, constant: 16),
            flashcardContentLabel.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 16),

            flashcardContentInput.topAnchor.constraint(equalTo: flashcardContentLabel.bottomAnchor, constant: 8),
            flashcardContentInput.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 16),
            flashcardContentInput.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: -16)

        ])
    }

    private func initData() {
        let flashcard = output.getFlashcard()
        flashcardTitleInput.text = flashcard.title
        flashcardContentInput.text = flashcard.content
    }

}

// MARK: Event methods
extension FlashcardEditView {
    @objc private func onCancelTap() {
        output.cancelEdit()
    }

    @objc private func onEditTap() {
        guard let item = navbar.items?.first else {
            return
        }

        loadingIndicator.startAnimating()
        item.rightBarButtonItem = loadingButtonItem

        output.updateFlashcard(title: flashcardTitleInput.text, content: flashcardContentInput.text)
    }
}

// MARK: Navigation bar delegate
extension FlashcardEditView: UINavigationBarDelegate {
}

// MARK: View input
extension FlashcardEditView: FlashcardEditViewInput {
    func flashcardUpdated() {
        loadingIndicator.stopAnimating()

        guard let item = navbar.items?.first else {
            return
        }
        item.rightBarButtonItem = editBarButtonItem
    }
}
