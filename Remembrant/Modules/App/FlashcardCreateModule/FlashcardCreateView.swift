//
//  FlashcardCreateView.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import UIKit

protocol FlashcardCreateViewInput: AnyObject {
    func flashcardCreated()
}

protocol FlashcardCreateViewOutput {
    func onCreateButtonClick(title: String?, content: String?)
    func cancelEdit()
}

// MARK: View
final class FlashcardCreateView: UIViewController {

    static let localizationPrefix = "flashcardCreate"

    // MARK: View Elements
    private lazy var navbar: UINavigationBar = {
        let navbar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 76))
        navbar.backgroundColor = .defaultModalBgColor
        navbar.delegate = self

        let navItem = UINavigationItem(title: NSLocalizedString("\(FlashcardCreateView.localizationPrefix)_title", comment: "Title for collection creation"))
        navItem.rightBarButtonItem = createBarButtonItem
        navItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(onCancelTap))

        navbar.items = [navItem]

        navbar.translatesAutoresizingMaskIntoConstraints = false

        return navbar
    }()

    private lazy var createBarButtonItem: UIBarButtonItem = {
        return UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(onCreateTap))
    }()

    private lazy var loadingBarButtonItem: UIBarButtonItem = {
        let barButton = UIBarButtonItem(customView: loadingIndicator)
        return barButton
    }()

    private lazy var loadingIndicator: UIActivityIndicatorView = {
        return UIActivityIndicatorView()
    }()

    private lazy var formStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.spacing = 10
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private lazy var flashcardTitleLabel: UILabel = {
        let label = UILabel()
        label.text = NSLocalizedString("\(FlashcardCreateView.localizationPrefix)_titleField_placeholder", comment: "Placeholder for flash card title input field")
        label.textColor = .secondaryTextColor
        label.font = .boldSystemFont(ofSize: UILabel.secondarySize)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var flashcardTitleInput: CustomTextField = {
        let input = CustomTextField()
        input.font = .systemFont(ofSize: UILabel.bodySize)
        input.textColor = .primaryTextColor
        input.backgroundColor = .inputBgColor
        input.layer.cornerRadius = 16
        input.translatesAutoresizingMaskIntoConstraints = false
        return input
    }()

    private lazy var flashcardContentLabel: UILabel = {
        let label = UILabel()
        label.text = NSLocalizedString("\(FlashcardCreateView.localizationPrefix)_valueField_placeholder", comment: "Placeholder for flashcard value input field")
        label.textColor = .secondaryTextColor
        label.font = .boldSystemFont(ofSize: UILabel.secondarySize)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var flashcardContentInput: CustomTextField = {
        let input = CustomTextField()
        input.font = .systemFont(ofSize: UILabel.bodySize)
        input.textColor = .primaryTextColor
        input.backgroundColor = .inputBgColor
        input.layer.cornerRadius = 16
        input.translatesAutoresizingMaskIntoConstraints = false
        return input
    }()

    // MARK: Dependencies
    var output: FlashcardCreateViewOutput!

    // MARK: Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .defaultModalBgColor

        initViews()
        initConstraints()

        flashcardTitleInput.becomeFirstResponder()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        resignFirstResponder()
    }

    // MARK: Init methods
    private func initViews() {
        view.addSubview(navbar)

        view.addSubview(flashcardTitleLabel)
        view.addSubview(flashcardTitleInput)
        view.addSubview(flashcardContentLabel)
        view.addSubview(flashcardContentInput)

    }

    private func initConstraints() {
        let safeArea = view.safeAreaLayoutGuide

        NSLayoutConstraint.activate([
            navbar.topAnchor.constraint(equalTo: safeArea.topAnchor),
            navbar.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            navbar.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),

            flashcardTitleLabel.topAnchor.constraint(equalTo: navbar.bottomAnchor, constant: 16),
            flashcardTitleLabel.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 16),

            flashcardTitleInput.topAnchor.constraint(equalTo: flashcardTitleLabel.bottomAnchor, constant: 8),
            flashcardTitleInput.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 16),
            flashcardTitleInput.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: -16),

            flashcardContentLabel.topAnchor.constraint(equalTo: flashcardTitleInput.bottomAnchor, constant: 16),
            flashcardContentLabel.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 16),

            flashcardContentInput.topAnchor.constraint(equalTo: flashcardContentLabel.bottomAnchor, constant: 8),
            flashcardContentInput.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 16),
            flashcardContentInput.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: -16)
        ])
    }
}

extension FlashcardCreateView: UINavigationBarDelegate {

}

// MARK: View events
extension FlashcardCreateView {
    @objc private func onCreateTap() {
        guard let item = navbar.items?.first else {
            return
        }
        loadingIndicator.startAnimating()
        item.rightBarButtonItem = loadingBarButtonItem
        output.onCreateButtonClick(title: flashcardTitleInput.text, content: flashcardContentInput.text)
    }

    @objc private func onCancelTap() {
        output.cancelEdit()
    }
}

extension FlashcardCreateView: FlashcardCreateViewInput {
    func flashcardCreated() {
        loadingIndicator.stopAnimating()

        guard let item = navbar.items?.first else {
            return
        }
        item.rightBarButtonItem = createBarButtonItem
    }
}
