//
//  CollectionItemCreationModule.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import UIKit

final class FlashcardCreateModule: Module {

    let moduleOutput: ModuleOutput? = nil

    func create(for navigationController: UINavigationController?, moduleInput: ModuleInput? = nil) -> UIViewController {
        let viewController = FlashcardCreateView()
        let configurator = FlashcardCreateConfigurator()
        configurator.configure(viewController: viewController, navigationController: navigationController, moduleInput: moduleInput)
        return viewController
    }
}
