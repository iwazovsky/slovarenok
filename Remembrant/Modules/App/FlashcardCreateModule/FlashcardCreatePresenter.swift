//
//  CollectionItemCreationPresenter.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import Foundation

// MARK: Presenter
final class FlashcardCreatePresenter {
    weak var view: FlashcardCreateViewInput!
    var interactor: FlashcardCreateInteractorInput!
    var router: FlashcardCreateRouterInput!

    var collection: Collection!
}

// MARK: View output
extension FlashcardCreatePresenter: FlashcardCreateViewOutput {
    func onCreateButtonClick(title: String?, content: String?) {

        // TODO: [CollectionItemCreation] Make input values validation before passing them to interactor
        guard let title = title else {
            return
        }

        guard let content = content else {
            return
        }

        DispatchQueue.global().async {
            self.interactor.createFlashcard(collection: self.collection, title: title, content: content)
        }
    }

    func cancelEdit() {
        router.cancelEdit()
    }

}

// MARK: Interactor output
extension FlashcardCreatePresenter: FlashcardCreateInteractorOutput {
    func flashcardCreated(_ flashcard: Flashcard) {
        var flashcards = self.collection.flashcards
        flashcards.append(flashcard)
        self.collection = Collection(id: self.collection.id, title: self.collection.title, flashcards: flashcards)

        DispatchQueue.main.sync {
            view.flashcardCreated()
            router.closeModal(flashcard: flashcard)
        }
    }
}
