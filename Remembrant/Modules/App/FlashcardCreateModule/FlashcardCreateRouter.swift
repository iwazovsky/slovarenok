//
//  CollectionItemCreationRouter.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import UIKit

protocol FlashcardCreateRouterInput {
    func closeModal(flashcard: Flashcard)
    func cancelEdit()
}

final class FlashcardCreateRouter: FlashcardCreateRouterInput {
    var navigationController: UINavigationController!
    var onDismiss: FlashcardCreateDismissCallback!

    func closeModal(flashcard: Flashcard) {
        navigationController.dismiss(animated: true)
        onDismiss(flashcard)
    }

    func cancelEdit() {
        navigationController.dismiss(animated: true)
    }
}
