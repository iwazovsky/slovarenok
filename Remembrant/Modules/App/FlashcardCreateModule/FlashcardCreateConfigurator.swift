//
//  CollectionItemCreationConfigurator.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import UIKit

final class FlashcardCreateConfigurator: Configurator {
    func configure(viewController: UIViewController, navigationController: UINavigationController?, moduleInput: ModuleInput?, moduleOutput: ModuleOutput?) {
        guard let viewController = viewController as? FlashcardCreateView else {
            fatalError("Wrong view controller passed to configurator")
        }
        guard let moduleInput = moduleInput as? FlashcardCreateModuleInput else {
            fatalError("Wrong module input passed to configurator")
        }

        let router = FlashcardCreateRouter()
        router.navigationController = navigationController
        router.onDismiss = moduleInput.onDismiss

        let interactor = FlashcardCreateInteractor()
        let flashcardProviderType = String(describing: FlashcardService.self)
        interactor.flashcardProvider = DIContainer.shared.resolve(flashcardProviderType)

        let presenter = FlashcardCreatePresenter()
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.collection = moduleInput.collection

        interactor.output = presenter

        viewController.output = presenter
    }
}
