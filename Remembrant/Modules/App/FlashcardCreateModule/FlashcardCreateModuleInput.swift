//
//  CollectionItemCreationModuleInput.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import UIKit

typealias FlashcardCreateDismissCallback = (_ collectionItem: Flashcard) -> Void

final class FlashcardCreateModuleInput: ModuleInput {

    let onDismiss: FlashcardCreateDismissCallback
    var collection: Collection

    init(collection: Collection, onDismiss: @escaping FlashcardCreateDismissCallback) {
        self.onDismiss = onDismiss
        self.collection = collection
    }
}
