//
//  CollectionItemCreationInteractor.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import Foundation

protocol FlashcardCreateInteractorInput {
    func createFlashcard(collection: Collection, title: String, content: String)
}

protocol FlashcardCreateInteractorOutput: AnyObject {
    func flashcardCreated(_ flashcard: Flashcard)
}

// MARK: Interactor
final class FlashcardCreateInteractor: FlashcardCreateInteractorInput {
    // MARK: Dependencies
    weak var output: FlashcardCreateInteractorOutput!
    var flashcardProvider: FlashcardProvider!

    func createFlashcard(collection: Collection, title: String, content: String) {
        let flashcard = Flashcard(id: 0, title: title, content: content)

        flashcardProvider.storeFlashcard(collection: collection, flashcard: flashcard) { (_ inner: CreateFlashcardThrowableCallback) in
            do {
                let flashcard = try inner()
                self.output.flashcardCreated(flashcard)
            } catch {
                print(error)
            }
        }
    }

}
