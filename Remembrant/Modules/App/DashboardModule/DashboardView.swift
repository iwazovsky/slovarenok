//
//  DashboardView.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 26.11.2022.
//

import UIKit

protocol DashboardViewInput: AnyObject {
}

protocol DashboardViewOutput {
    func navigateToCollections()
    func navigateToSettings()
}

// MARK: View
final class DashboardView: UIViewController {

    // MARK: View elements
    private lazy var welcomeTitle: UILabel = {
        let label = UILabel()
        label.text = "\(greetingRandomizer?.getRandomizedGreeting() ?? ""),"
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.font = .boldSystemFont(ofSize: UILabel.superBigSize)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var userGreetingView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private lazy var pillView: DashboardPillView = {
        let view = DashboardPillView()
        view.backgroundColor = .clear
        view.transform = CGAffineTransform(rotationAngle: CGFloat.pi*1.025)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 40)
        label.textColor = .primaryTextColor
        label.text = "Константин"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var menuView: UIView = {
        let view = UIView()
        view.isUserInteractionEnabled = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    // MARK: Collections menu item
    private lazy var goToCollectionsStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.alignment = .center
        stack.spacing = 8
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()

    private lazy var goToCollectionsButton: UIButton = {
        let button = UIButton()
        button.setTitle(NSLocalizedString("dashboard_goToCollections", comment: "Go to location button text"), for: .normal)
        button.setTitleColor(.primaryTextColor, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 17)
        button.contentHorizontalAlignment = .left
        button.addTarget(self, action: #selector(navigateToCollections), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private lazy var goToCollectionsIcon: UIImageView = {
        let image = UIImageView()
        image.image = .arrowForwardIcon
        return image
    }()

    // MARK: Settings menu item
    private lazy var goToSettingsStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.alignment = .center
        stack.spacing = 8
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()

    private lazy var goToSettingsButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(.primaryTextColor, for: .normal)
        button.setTitle(NSLocalizedString("dashboard_goToSettings", comment: "Go to settings button text"), for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 17)
        button.contentHorizontalAlignment = .left
        button.addTarget(self, action: #selector(navigateToSettings), for: .touchUpInside)
        return button
    }()

    private lazy var goToSettingsIcon: UIImageView = {
        let image = UIImageView()
        image.image = .arrowForwardIcon
        return image
    }()

    private lazy var dividerView: DividerView = {
        let view = DividerView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    // MARK: Dependencies
    var greetingRandomizer: GreetingRandomizer!
    var output: DashboardViewOutput!

    // MARK: Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .defaultBgColor

        initViews()
        initConstraints()
    }

    // MARK: Init methods
    private func initViews() {
        view.addSubview(welcomeTitle)
        view.addSubview(userGreetingView)
        view.addSubview(menuView)

        menuView.addSubview(goToCollectionsStackView)
        goToCollectionsStackView.addArrangedSubview(goToCollectionsButton)
        goToCollectionsStackView.addArrangedSubview(goToCollectionsIcon)
        menuView.addSubview(dividerView)
        menuView.addSubview(goToSettingsStackView)
        goToSettingsStackView.addArrangedSubview(goToSettingsButton)
        goToSettingsStackView.addArrangedSubview(goToSettingsIcon)

        userGreetingView.addSubview(pillView)
        userGreetingView.addSubview(nameLabel)

    }

    private func initConstraints() {
        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            welcomeTitle.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: 16),
            welcomeTitle.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 16),
            welcomeTitle.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: -16),

            userGreetingView.topAnchor.constraint(equalTo: welcomeTitle.bottomAnchor, constant: 16),
            userGreetingView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 16),
            userGreetingView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: -16),
            userGreetingView.heightAnchor.constraint(equalToConstant: 112),

            pillView.topAnchor.constraint(equalTo: userGreetingView.topAnchor),
            pillView.bottomAnchor.constraint(equalTo: userGreetingView.bottomAnchor),
            pillView.leadingAnchor.constraint(equalTo: userGreetingView.leadingAnchor),
            pillView.trailingAnchor.constraint(equalTo: userGreetingView.trailingAnchor),

            nameLabel.centerXAnchor.constraint(equalTo: userGreetingView.centerXAnchor),
            nameLabel.centerYAnchor.constraint(equalTo: userGreetingView.centerYAnchor),

            menuView.topAnchor.constraint(equalTo: userGreetingView.bottomAnchor, constant: 140),
            menuView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 16),
            menuView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: -16),
            menuView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: -16),

//            menuView.centerYAnchor.constraint(equalTo: safeArea.centerYAnchor, constant: 20),
//            menuView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 16),
//            menuView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: -16),

            goToCollectionsStackView.topAnchor.constraint(equalTo: menuView.topAnchor),
            goToCollectionsStackView.leadingAnchor.constraint(equalTo: menuView.leadingAnchor),
            goToCollectionsStackView.trailingAnchor.constraint(equalTo: menuView.trailingAnchor),

            dividerView.topAnchor.constraint(equalTo: goToCollectionsStackView.bottomAnchor, constant: 8),
            dividerView.leadingAnchor.constraint(equalTo: menuView.leadingAnchor),
            dividerView.trailingAnchor.constraint(equalTo: menuView.trailingAnchor),
            dividerView.heightAnchor.constraint(equalToConstant: 1),

            goToSettingsStackView.topAnchor.constraint(equalTo: dividerView.bottomAnchor, constant: 8),
            goToSettingsStackView.leadingAnchor.constraint(equalTo: menuView.leadingAnchor),
            goToSettingsStackView.trailingAnchor.constraint(equalTo: menuView.trailingAnchor),

            goToCollectionsIcon.widthAnchor.constraint(equalToConstant: 17),
            goToCollectionsIcon.heightAnchor.constraint(equalToConstant: 17),
            goToSettingsIcon.widthAnchor.constraint(equalToConstant: 17),
            goToSettingsIcon.heightAnchor.constraint(equalToConstant: 17)

//            menuView.topAnchor.constraint(equalTo: welcomeTitle)
        ])
    }
}

// MARK: Event methods
extension DashboardView {
    @objc private func navigateToCollections() {
        output.navigateToCollections()
    }

    @objc private func navigateToSettings() {
        output.navigateToSettings()
    }
}

extension DashboardView: DashboardViewInput {

}
