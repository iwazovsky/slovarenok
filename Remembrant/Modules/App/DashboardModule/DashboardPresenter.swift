//
//  DashboardPresenter.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 26.11.2022.
//

import Foundation

final class DashboardPresenter {
    weak var view: DashboardViewInput!
    var interactor: DashboardInteractorInput!
    var router: DashboardRouterInput!

}

extension DashboardPresenter: DashboardViewOutput {
    func navigateToCollections() {
        router.navigateToCollections()
    }

    func navigateToSettings() {
        router.navigateToSettings()
    }
}
