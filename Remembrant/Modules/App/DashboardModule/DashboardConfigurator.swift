//
//  DashboardConfigurator.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 26.11.2022.
//

import UIKit

final class DashboardConfigurator: Configurator {
    func configure(viewController: UIViewController, navigationController: UINavigationController?, moduleInput: ModuleInput?, moduleOutput: ModuleOutput?) {
        guard let viewController = viewController as? DashboardView else { return }

        let router = DashboardRouter()
        router.navigationController = navigationController

        let interactor = DashboardInteractor()

        let presenter = DashboardPresenter()
        presenter.interactor = interactor
        presenter.router = router
        presenter.view = viewController

        viewController.output = presenter
        viewController.greetingRandomizer = DIContainer.shared.resolve()
    }
}
