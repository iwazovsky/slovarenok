//
//  DashboardRouter.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 26.11.2022.
//

import UIKit

protocol DashboardRouterInput {
    func navigateToCollections()
    func navigateToSettings()
}

final class DashboardRouter: DashboardRouterInput {
    var navigationController: UINavigationController!

    func navigateToCollections() {
        let module = CollectionsListModule()
        let viewController = module.create(for: navigationController, moduleInput: nil)
        navigationController.pushViewController(viewController, animated: true)
    }

    func navigateToSettings() {
        let module = SettingsModule()
        let viewController = module.create(for: navigationController, moduleInput: nil)
        navigationController.pushViewController(viewController, animated: true)
    }
}
