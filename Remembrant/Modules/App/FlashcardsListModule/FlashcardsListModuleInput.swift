//
//  CollectionModuleInput.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import Foundation

final class FlashcardsListModuleInput: ModuleInput {
    var collection: Collection
    var showFlashcardActionsPopup: ShowFlashcardActionsPopupCallback

    init(collection: Collection, showFlashcardActionsPopup: @escaping ShowFlashcardActionsPopupCallback) {
        self.collection = collection
        self.showFlashcardActionsPopup = showFlashcardActionsPopup
    }
}
