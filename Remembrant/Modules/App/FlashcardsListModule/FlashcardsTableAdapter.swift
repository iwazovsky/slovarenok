//
//  FlashcardsTableAdapter.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import UIKit

// MARK: Adapter
final class FlashcardTableAdapter: NSObject {
    var flashcards: [Flashcard] = []
    weak var viewInput: FlashcardsListViewInput!
    var showFlashcardActionsPopup: ShowFlashcardActionsPopupCallback!

    private let defaultEstimatedRowHeight: CGFloat = 32
}

// MARK: TableView Delegate
extension FlashcardTableAdapter: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return defaultEstimatedRowHeight
    }

}

// MARK: TableView DataSource
extension FlashcardTableAdapter: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? FlashcardsTableViewCell else {
            fatalError("Wrong cell passed to table view adapter")
        }

        cell.flashcard = flashcards[indexPath.row]
        cell.selectionStyle = .none
        cell.doUpdates = {
            print("Do updates")
            tableView.beginUpdates()
            tableView.endUpdates()
        }

        cell.showFlashcardActionsPopup = showFlashcardActionsPopup

        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return flashcards.count
    }

    func hydrate(tableView: UITableView, flashcards: [Flashcard]) {
        self.flashcards = flashcards
        tableView.reloadData()
    }

    func insertRow(tableView: UITableView, flashcard: Flashcard) {
        flashcards.insert(flashcard, at: 0)

        tableView.beginUpdates()
        tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
        tableView.endUpdates()
    }

    func deleteRow(tableView: UITableView, flashcard: Flashcard) {
        guard let index = flashcards.firstIndex(of: flashcard) else {
            // TODO: Add logs and handler on deleted in tableView
            print("Collection item not found")
            return
        }
        flashcards.remove(at: index)

        tableView.beginUpdates()
        tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
        tableView.endUpdates()
    }

    func updateRow(tableView: UITableView, flashcard: Flashcard) {
        guard let index = flashcards.firstIndex(of: flashcard) else {
            // TODO: Add logs and handler on edit in tableView
            print("Collection item not found")
            return
        }

        flashcards[index] = flashcard

        tableView.beginUpdates()
        tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
        tableView.endUpdates()
    }
}
