//
//  FlashcardsListModuleOutput.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 04.01.2023.
//

import Foundation

class FlashcardsListModuleOutput: ModuleOutput {
    var flashcardEdited: ((Flashcard) -> Void)?
    var flashcardCreated: ((Flashcard) -> Void)?
    var flashcardDeleted: ((Flashcard) -> Void)?
}
