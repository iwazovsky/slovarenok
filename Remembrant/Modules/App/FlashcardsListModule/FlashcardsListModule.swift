//
//  CollectionModule.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import UIKit

final class FlashcardsListModule: Module {

    let moduleOutput: ModuleOutput?

    init() {
        self.moduleOutput = FlashcardsListModuleOutput()
    }

    func create(for navigationController: UINavigationController?, moduleInput: ModuleInput?) -> UIViewController {
        let viewController = FlashcardsListView()
        let configurator = FlashcardsListConfigurator()
        configurator.configure(viewController: viewController, navigationController: navigationController, moduleInput: moduleInput, moduleOutput: moduleOutput)
        return viewController
    }
}
