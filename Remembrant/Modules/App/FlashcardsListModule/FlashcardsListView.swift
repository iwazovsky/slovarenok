//
//  CollectionView.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import UIKit

protocol FlashcardsListViewInput: AnyObject {
    func onFetchedFlashcards(_ items: [Flashcard])
    func flashcardCreated(_ flashcard: Flashcard)
    func flashcardEdited(_ flashcard: Flashcard)
    func flashcardDeleted(_ flashcard: Flashcard)
}

protocol FlashcardsListViewOutput {
    func fetchFlashcards()
}

// MARK: View
final class FlashcardsListView: UIViewController {

    // MARK: View elements
    private lazy var flashcardsTableView: UITableView = {
        let table = UITableView()
        table.delegate = tableViewAdapter
        table.dataSource = tableViewAdapter
        tableViewAdapter.showFlashcardActionsPopup = showFlashcardActionsPopup
        table.register(FlashcardsTableViewCell.self, forCellReuseIdentifier: "cell")
        table.separatorStyle = .none
        table.rowHeight = UITableView.automaticDimension
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()

    // MARK: Dependencies
    var output: FlashcardsListViewOutput!
    var tableViewAdapter: FlashcardTableAdapter!
    var showFlashcardActionsPopup: ShowFlashcardActionsPopupCallback!

    // MARK: Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .defaultBgColor

        initViews()
        initConstraints()
        initData()
    }

    // MARK: View init methods
    private func initViews() {
        view.addSubview(flashcardsTableView)
    }

    private func initConstraints() {
        let safeArea = view.safeAreaLayoutGuide

        NSLayoutConstraint.activate([
            flashcardsTableView.topAnchor.constraint(equalTo: safeArea.topAnchor),
            flashcardsTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            flashcardsTableView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            flashcardsTableView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor)
        ])
    }

    private func initData() {
        output.fetchFlashcards()
    }
}

// MARK: View events
extension  FlashcardsListView {
}

// MARK: View input realisation
extension FlashcardsListView: FlashcardsListViewInput {
    func onFetchedFlashcards(_ flashcards: [Flashcard]) {
        tableViewAdapter.hydrate(tableView: flashcardsTableView, flashcards: flashcards)
    }

    func flashcardCreated(_ flashcard: Flashcard) {
        tableViewAdapter.insertRow(tableView: flashcardsTableView, flashcard: flashcard)
    }

    func flashcardDeleted(_ flashcard: Flashcard) {
        tableViewAdapter.deleteRow(tableView: flashcardsTableView, flashcard: flashcard)
    }

    func flashcardEdited(_ flashcard: Flashcard) {
        tableViewAdapter.updateRow(tableView: flashcardsTableView, flashcard: flashcard)
    }
}
