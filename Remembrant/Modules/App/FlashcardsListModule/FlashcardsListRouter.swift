//
//  CollectionRouter.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import UIKit

protocol FlashcardsListRouterInput {
}

final class FlashcardsListRouter: FlashcardsListRouterInput {

    var navigationController: UINavigationController!

}
