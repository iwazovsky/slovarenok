//
//  CollectionInteractor.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import Foundation

protocol FlashcardsListInteractorInput {
    func fetchFlashcards(collection: Collection)
}

protocol FlashcardsListInteractorOutput: AnyObject {
    func flashcardsFetched(_ flashcards: [Flashcard])
}

final class FlashcardsListInteractor: FlashcardsListInteractorInput {
    weak var output: FlashcardsListInteractorOutput!
    var flashcardProvider: FlashcardProvider!

    func fetchFlashcards(collection: Collection) {
        flashcardProvider.fetchFlashcards(collection: collection) { (_ inner: GetFlashcardsThrowableCallback) in
            do {
                let flashcards = try inner()
                self.output.flashcardsFetched(flashcards)
            } catch {
                print(error.localizedDescription)
            }
        }
    }
}
