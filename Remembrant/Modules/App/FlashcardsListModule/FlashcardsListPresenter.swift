//
//  CollectionPresenter.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import Foundation

// MARK: Presenter
final class FlashcardsListPresenter {
    weak var view: FlashcardsListViewInput!
    var interactor: FlashcardsListInteractorInput!
    var router: FlashcardsListRouterInput!

    var collection: Collection!

    func flashcardCreated(_ flashcard: Flashcard) {
        view.flashcardCreated(flashcard)
    }

    func flashcardEdited(_ flashcard: Flashcard) {
        view.flashcardEdited(flashcard)
    }

    func flashcardDeleted(_ flashcard: Flashcard) {
        view.flashcardDeleted(flashcard)
    }
}

// MARK: View output
extension FlashcardsListPresenter: FlashcardsListViewOutput {
    func getCollection() -> Collection {
        return collection
    }

    func fetchFlashcards() {
        DispatchQueue.global().async {
            self.interactor.fetchFlashcards(collection: self.collection)
        }
    }

}

// MARK: Interactor output
extension FlashcardsListPresenter: FlashcardsListInteractorOutput {
    func flashcardsFetched(_ flashcards: [Flashcard]) {
        DispatchQueue.main.sync {
            view.onFetchedFlashcards(flashcards)
        }
    }

}
