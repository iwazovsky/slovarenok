//
//  CollectionConfigurator.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import UIKit

final class FlashcardsListConfigurator: Configurator {
    func configure(viewController: UIViewController, navigationController: UINavigationController?, moduleInput: ModuleInput?, moduleOutput: ModuleOutput?) {
        guard let viewController = viewController as? FlashcardsListView else {
            fatalError("Wrong view passed to configurator")
        }

        guard let moduleInput = moduleInput as? FlashcardsListModuleInput else {
            fatalError("Wrong module input passed to configurator")
        }

        let router = FlashcardsListRouter()
        router.navigationController = navigationController

        let interactor = FlashcardsListInteractor()

        let presenter = FlashcardsListPresenter()
        presenter.router = router
        presenter.interactor = interactor
        presenter.view = viewController
        presenter.collection = moduleInput.collection

        interactor.output = presenter
        let flashcardProviderType = String(describing: FlashcardService.self)
        interactor.flashcardProvider = DIContainer.shared.resolve(flashcardProviderType)

        let adapter = FlashcardTableAdapter()
        adapter.viewInput = viewController

        viewController.tableViewAdapter = adapter
        viewController.output = presenter
        viewController.showFlashcardActionsPopup = moduleInput.showFlashcardActionsPopup

        guard let moduleOutput = moduleOutput as? FlashcardsListModuleOutput else {
            fatalError("Wrong module output passed to FlashcardsListConfigurator")
        }

        moduleOutput.flashcardEdited = presenter.flashcardEdited
        moduleOutput.flashcardCreated = presenter.flashcardCreated
        moduleOutput.flashcardDeleted = presenter.flashcardDeleted
    }
}
