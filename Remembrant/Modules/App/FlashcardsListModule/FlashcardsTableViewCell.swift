//
//  File.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import UIKit

// MARK: View
final class FlashcardsTableViewCell: UITableViewCell {

    // MARK: View Elements
    private lazy var wrapperView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .primaryTextColor
        label.font = .boldSystemFont(ofSize: 17)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var contentLabel: UILabel = {
        let label = UILabel()
        label.textColor = .secondaryTextColor
        label.font = .systemFont(ofSize: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var moreMenuBtn: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(.moreMenuIcon, for: .normal)
        button.addTarget(self, action: #selector(openMoreMenu), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private lazy var expandMenuBtn: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(.expandMoreIcon, for: .normal)
        button.addTarget(self, action: #selector(toggleFlashcard), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private lazy var headerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fillProportionally
        stackView.alignment = .center
        stackView.axis = .horizontal
        stackView.spacing = 16
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    private lazy var trackedConstraints: [String: NSLayoutConstraint] = [
        "headerStackView_bottom": headerStackView.bottomAnchor.constraint(equalTo: wrapperView.bottomAnchor, constant: -8)
    ]

    // MARK: Dependencies
    var flashcard: Flashcard? {
        didSet {
            guard let flashcard = flashcard else { return }

            titleLabel.text = flashcard.title
            contentLabel.text = flashcard.content
        }
    }

    var showFlashcardActionsPopup: ShowFlashcardActionsPopupCallback!
    var doUpdates: (() -> Void)!

    required init?(coder: NSCoder) {
        fatalError("Storyboards are not supported")
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: "cell")

        initViews()
        initConstraints()
    }

    // MARK: Init methods
    private func initViews() {
        contentView.addSubview(wrapperView)

        wrapperView.addSubview(headerStackView)
        headerStackView.addArrangedSubview(titleLabel)
        headerStackView.addArrangedSubview(expandMenuBtn)
        headerStackView.addArrangedSubview(moreMenuBtn)

        wrapperView.backgroundColor = .defaultModalBgColor
        wrapperView.layer.cornerRadius = 16

    }

    private func initConstraints() {
        NSLayoutConstraint.activate([
            wrapperView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            wrapperView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
            wrapperView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            wrapperView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),

            headerStackView.topAnchor.constraint(equalTo: wrapperView.topAnchor, constant: 8),
            headerStackView.trailingAnchor.constraint(equalTo: wrapperView.trailingAnchor, constant: -16),
            headerStackView.leadingAnchor.constraint(equalTo: wrapperView.leadingAnchor, constant: 16),

            moreMenuBtn.widthAnchor.constraint(equalToConstant: 24),
            moreMenuBtn.heightAnchor.constraint(equalToConstant: 24),

            expandMenuBtn.widthAnchor.constraint(equalToConstant: 24),
            expandMenuBtn.heightAnchor.constraint(equalToConstant: 24)

        ])

        trackedConstraints["headerStackView_bottom"] = headerStackView.bottomAnchor.constraint(equalTo: wrapperView.bottomAnchor, constant: -8)
        trackedConstraints["headerStackView_bottom"]?.isActive = true
    }
}

// MARK: Event methods
extension FlashcardsTableViewCell {
    @objc private func toggleFlashcard() {
        guard let constraint = trackedConstraints["headerStackView_bottom"] else {
            return
        }

        if constraint.isActive {
            expandMenuBtn.setImage(.expandLessIcon, for: .normal)

            constraint.isActive = false
            wrapperView.addSubview(contentLabel)

            NSLayoutConstraint.activate([
                contentLabel.topAnchor.constraint(equalTo: headerStackView.bottomAnchor, constant: 8),
                contentLabel.trailingAnchor.constraint(equalTo: wrapperView.trailingAnchor, constant: -16),
                contentLabel.leadingAnchor.constraint(equalTo: wrapperView.leadingAnchor, constant: 16),
                contentLabel.bottomAnchor.constraint(equalTo: wrapperView.bottomAnchor, constant: -8)
            ])

        } else {
            expandMenuBtn.setImage(.expandMoreIcon, for: .normal)
            constraint.isActive = true
            contentLabel.removeFromSuperview()
        }

        doUpdates()

    }

    @objc private func openMoreMenu() {
        guard let flashcard = flashcard else { return }

        showFlashcardActionsPopup(flashcard)
    }
}
