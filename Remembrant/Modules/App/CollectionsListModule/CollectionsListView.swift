//
//  CollectionsListView.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 26.11.2022.
//

import UIKit

protocol CollectionsListViewInput: AnyObject {
    func navigateToCollection(_ collection: Collection)
    func hydrateCollectionsList(_ collections: [Collection])

    // MARK: View input create collection requirements
    func onNewCollection(_ collection: Collection)

    // MARK: View input delete collection requirements
    func deleteCollection(_ collection: Collection)
    func onDeletedCollection(_ collection: Collection)

    // MARK: View input update collection requirements
    func collectionUpdated(_ collection: Collection)
}

protocol CollectionsListViewOutput {
    func fetchCollections()
    func navigateToCollection(_ collection: Collection)
    func onAddCollectionButtonClick()
    func deleteCollection(_ collection: Collection)
}

// MARK: View
final class CollectionsListView: UIViewController {

    // MARK: View elements
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = NSLocalizedString("collections_title", comment: "Title for collections tab")
        label.textColor = .primaryTextColor
        label.font = .boldSystemFont(ofSize: 34)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var collectionsTableView: UITableView = {
        let view = UITableView()
        view.delegate = collectionsListTableViewAdapter
        view.dataSource = collectionsListTableViewAdapter
        view.register(CollectionsListTableViewCell.self, forCellReuseIdentifier: "cell")
        view.separatorColor = .primaryColor
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private lazy var addCollectionButton: UIBarButtonItem = {
        let button = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(onAddCollectionButtonClick))
        return button
    }()

    private lazy var loadingIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.translatesAutoresizingMaskIntoConstraints = false
        return indicator
    }()

    // MARK: Dependencies
    var output: CollectionsListViewOutput!
    var collectionsListTableViewAdapter: CollectionsListTableViewAdapter!

    // MARK: Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .defaultBgColor

        initViews()
        initConstraints()
        initData()
    }

    // MARK: Init methods
    private func initViews() {
        navigationItem.rightBarButtonItem = addCollectionButton
        view.addSubview(titleLabel)
        view.addSubview(loadingIndicator)
        view.addSubview(collectionsTableView)
    }

    private func initConstraints() {
        let safeArea = view.safeAreaLayoutGuide

        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: 16),
            titleLabel.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 16),

            loadingIndicator.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor),
            loadingIndicator.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor, constant: 16),
            loadingIndicator.heightAnchor.constraint(equalToConstant: 17),
            loadingIndicator.widthAnchor.constraint(equalToConstant: 17),

            collectionsTableView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 12),
            collectionsTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            collectionsTableView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            collectionsTableView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor)
        ])
    }

    private func initData() {
        loadingIndicator.startAnimating()
        output.fetchCollections()
    }

}

// MARK: View Events
extension CollectionsListView {
    @objc private func onAddCollectionButtonClick() {
        output.onAddCollectionButtonClick()
    }
}

// MARK: View Input
extension CollectionsListView: CollectionsListViewInput {

    func hydrateCollectionsList(_ collections: [Collection]) {
        loadingIndicator.stopAnimating()
        collectionsListTableViewAdapter.hydrate(tableView: collectionsTableView, collections)
    }

    func navigateToCollection(_ collection: Collection) {
        output.navigateToCollection(collection)
    }

    func onNewCollection(_ collection: Collection) {
        collectionsListTableViewAdapter.insertRow(tableView: collectionsTableView, collection)
    }

    func deleteCollection(_ collection: Collection) {
        let cancelAction = UIAlertAction(title: NSLocalizedString("system_cancel", comment: "Cancel action"), style: .cancel)
        let deleteAction = UIAlertAction(title: NSLocalizedString("system_delete", comment: "Delete action"), style: .destructive) { [weak self] _ in
            self? .output.deleteCollection(collection)
        }
        let alert = UIAlertController(title: NSLocalizedString("collectionsList_deleteConfirmation", comment: "Title for delete dialog"), message: collection.title, preferredStyle: .alert)
        alert.addAction(cancelAction)
        alert.addAction(deleteAction)

        present(alert, animated: true)

//        output.deleteCollection(collection)
    }

    func onDeletedCollection(_ collection: Collection) {
        collectionsListTableViewAdapter.deleteRow(tableView: collectionsTableView, collection)
    }

    func collectionUpdated(_ collection: Collection) {
        collectionsListTableViewAdapter.updateRow(tableView: collectionsTableView, collection)
    }
}
