//
//  CollectionListConfigurator.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 26.11.2022.
//

import UIKit

final class CollectionsListConfigurator: Configurator {

    func configure(viewController: UIViewController, navigationController: UINavigationController?, moduleInput: ModuleInput?, moduleOutput: ModuleOutput?) {
        guard let viewController = viewController as? CollectionsListView else { return }

        let interactor = CollectionsListInteractor()
        let collectionProviderType = String(describing: CollectionService.self)
        interactor.collectionService = DIContainer.shared.resolve(collectionProviderType)

        let router = CollectionsListRouter()
        router.navigationController = navigationController

        let presenter = CollectionsListPresenter()
        presenter.interactor = interactor
        presenter.view = viewController
        presenter.router = router

        interactor.output = presenter

        let tableAdapter = CollectionsListTableViewAdapter()
        tableAdapter.viewInput = viewController
        viewController.collectionsListTableViewAdapter = tableAdapter

        viewController.output = presenter
    }

}
