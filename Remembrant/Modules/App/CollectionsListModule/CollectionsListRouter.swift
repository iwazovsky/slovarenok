//
//  CollectionsListRouter.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 26.11.2022.
//

import UIKit

protocol CollectionsListRouterInput {
    func navigateToCollection(_ collection: Collection, onDismiss: @escaping CollectionDismissCallback)
    func navigateToCollectionCreate(onDismiss: @escaping CollectionCreateDismissCallback)
}

final class CollectionsListRouter: CollectionsListRouterInput {
    var navigationController: UINavigationController!

    func navigateToCollection(_ collection: Collection, onDismiss: @escaping CollectionDismissCallback) {
        let module = CollectionModule()
        let moduleInput = CollectionModuleInput(collection: collection, onDismiss: onDismiss)
        let viewController = module.create(for: navigationController, moduleInput: moduleInput)
        navigationController.pushViewController(viewController, animated: true)
    }

    func navigateToCollectionCreate(onDismiss: @escaping CollectionCreateDismissCallback) {
        let module = CollectionCreateModule()
        let moduleInput = CollectionCreateModuleInput(onDismiss: onDismiss)
        let viewController = module.create(for: navigationController, moduleInput: moduleInput)
        navigationController.present(viewController, animated: true)
    }
}
