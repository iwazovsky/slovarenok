//
//  CollectionsListTableViewAdapter.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import UIKit

final class CollectionsListTableViewAdapter: NSObject {
    var collections: [Collection] = []
    weak var viewInput: CollectionsListViewInput!

    private let defaultEstimatedRowHeight: CGFloat = 32
}

extension CollectionsListTableViewAdapter: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let collection = collections[indexPath.row]
        viewInput.navigateToCollection(collection)
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return defaultEstimatedRowHeight
    }
}

extension CollectionsListTableViewAdapter: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return collections.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? CollectionsListTableViewCell else {
            fatalError("CollectionsListTableView cell is not the one as expected")
        }

        cell.collection = collections[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            viewInput.deleteCollection(collections[indexPath.row])
        }
    }

    func hydrate(tableView: UITableView, _ collections: [Collection]) {
        self.collections = collections
        tableView.reloadData()
    }

    func deleteRow(tableView: UITableView, _ collection: Collection) {
        guard let index = collections.firstIndex(of: collection) else {
            // TODO: Add logs and handler on deleted in tableView
            print("Collection not found")
            return
        }

        collections.remove(at: index)

        tableView.beginUpdates()
        tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
        tableView.endUpdates()
    }

    func insertRow(tableView: UITableView, _ collection: Collection) {
        collections.insert(collection, at: 0)

        tableView.beginUpdates()
        tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
        tableView.endUpdates()

    }

    func updateRow(tableView: UITableView, _ collection: Collection) {
        guard let index = collections.firstIndex(of: collection) else {
            // TODO: Add logs and handler on edit in Collection tableView
            print("Collection not found")
            return
        }

        collections[index] = collection
        tableView.beginUpdates()
        tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
        tableView.endUpdates()
    }
}
