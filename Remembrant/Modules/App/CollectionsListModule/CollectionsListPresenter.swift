//
//  CollectionsListPresenter.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 26.11.2022.
//

import Foundation

// MARK: Presenter
final class CollectionsListPresenter {

    weak var view: CollectionsListViewInput!
    var interactor: CollectionsListInteractorInput!
    var router: CollectionsListRouterInput!

    var collections: [Collection]? = []

    private func onCreatedCollectionReceive(collection: Collection) {
        self.collections?.append(collection)
        view.onNewCollection(collection)
    }

    private func onCollectionDismiss(collection: Collection) {
        guard let index = collections?.firstIndex(of: collection) else {
            // TODO: Handle error on collection dismiss
            print("Collection not found")
            return
        }
        self.collections![index] = collection
        view.collectionUpdated(collection)
    }
}

// MARK: View output
extension CollectionsListPresenter: CollectionsListViewOutput {
    func navigateToCollection(_ collection: Collection) {
        router.navigateToCollection(collection, onDismiss: onCollectionDismiss)
    }

    func fetchCollections() {
        DispatchQueue.global().async {
            self.interactor.fetchCollections()
        }
    }

    func onAddCollectionButtonClick() {
        router.navigateToCollectionCreate(onDismiss: onCreatedCollectionReceive)
    }

    func deleteCollection(_ collection: Collection) {
        DispatchQueue.global().async {
            self.interactor.deleteCollection(collection)
        }
    }
}

// MARK: Interactor output
extension CollectionsListPresenter: CollectionsListInteractorOutput {

    func onFetchCollections(_ collections: [Collection]) {
        self.collections = collections

        DispatchQueue.main.sync {
            self.view.hydrateCollectionsList(collections)
        }
    }

    func onDeletedCollection(_ collection: Collection) {
        guard let index = self.collections?.firstIndex(of: collection) else {
            // TODO: Add logs and handler on deleted event in presenter
            print("Collection not found")
            return
        }
        self.collections?.remove(at: index)

        DispatchQueue.main.sync {
            view.onDeletedCollection(collection)
        }
    }
}
