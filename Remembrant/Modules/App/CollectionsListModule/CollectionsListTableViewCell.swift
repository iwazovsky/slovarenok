//
//  CollectionsListTableViewCell.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 15.12.2022.
//

import UIKit

final class CollectionsListTableViewCell: UITableViewCell {

    var collection: Collection? {
        didSet {
            guard let collection = collection else { return }

            titleLabel.text = collection.title

            totalBadge.text = "\(NSLocalizedString("collectionsList_cards", comment: "title for cards")) \(collection.flashcards.count)"
            titleLabel.setNeedsLayout()
        }
    }

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .primaryTextColor
        label.font = .systemFont(ofSize: UILabel.bodySize)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var totalBadgeView: UIView = {
        let view = UIView()
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.primaryColor.cgColor
        view.layer.cornerRadius = 9
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private lazy var totalBadge: UILabel = {
        let label = UILabel()
        label.textColor = .primaryTextColor
        label.text = "Cards 46"
        label.font = .systemFont(ofSize: UILabel.smallCaptionsSize)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var forwardIcon: UIImageView = {
        let icon = UIImageView()
        icon.image = .arrowForwardIcon
        icon.translatesAutoresizingMaskIntoConstraints = false
        return icon
    }()

    private lazy var headerStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.alignment = .center
        stack.spacing = 8
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()

    required init?(coder: NSCoder) {
        fatalError("Storyboards are not supported")
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        initViews()
        initConstraints()
    }

    private func initViews() {
        contentView.addSubview(headerStackView)
        contentView.addSubview(totalBadgeView)
        totalBadgeView.addSubview(totalBadge)

        headerStackView.addArrangedSubview(titleLabel)
        headerStackView.addArrangedSubview(forwardIcon)
    }

    private func initConstraints() {
        NSLayoutConstraint.activate([
            headerStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 12),
            headerStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            headerStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),

            forwardIcon.heightAnchor.constraint(equalToConstant: 16),
            forwardIcon.widthAnchor.constraint(equalToConstant: 16),

            totalBadgeView.topAnchor.constraint(equalTo: headerStackView.bottomAnchor, constant: 8),
            totalBadgeView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),
            totalBadgeView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            totalBadgeView.heightAnchor.constraint(greaterThanOrEqualTo: totalBadge.heightAnchor, constant: 8),
            totalBadgeView.widthAnchor.constraint(greaterThanOrEqualTo: totalBadge.widthAnchor, constant: 16),

            totalBadge.centerXAnchor.constraint(equalTo: totalBadgeView.centerXAnchor),
            totalBadge.centerYAnchor.constraint(equalTo: totalBadgeView.centerYAnchor)
        ])
    }
}
