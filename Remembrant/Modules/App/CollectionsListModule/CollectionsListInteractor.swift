//
//  CollectionsListInteractor.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 26.11.2022.
//

import Foundation

protocol CollectionsListInteractorInput {
    func fetchCollections()
    func deleteCollection(_ collection: Collection)
}

protocol CollectionsListInteractorOutput: AnyObject {
    func onFetchCollections(_ collections: [Collection])
    func onDeletedCollection(_ collection: Collection)
}

// MARK: Interactor
final class CollectionsListInteractor {
    weak var output: CollectionsListInteractorOutput!
    var collectionService: CollectionProvider!
}

// MARK: Input
extension CollectionsListInteractor: CollectionsListInteractorInput {

    func fetchCollections() {
        self.collectionService.getCollections { (inner: GetCollectionThrowableCallback) in
            do {
                let collections = try inner()
                self.output.onFetchCollections(collections ?? [])
            } catch {
//                    let errorCode = (error as NSError).code
                print(error)
            }
        }
    }

    func deleteCollection(_ collection: Collection) {
        self.collectionService.deleteCollection(collection) { (inner: DeleteCollectionThrowableCallback) in
            do {
                try inner()
                self.output.onDeletedCollection(collection)
            } catch {
                print(error)
            }
        }
    }
}
