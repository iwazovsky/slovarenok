//
//  CollectionView.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 02.01.2023.
//

import UIKit

typealias ShowFlashcardActionsPopupCallback = (Flashcard) -> Void

protocol CollectionViewInput: AnyObject {
    func onCollectionTitleEdit()
}

protocol CollectionViewOutput: AnyObject {
    func getCollection() -> Collection
    func editCollectionTitle(title: String?)

    func openEditFlashcardPopup(flashcard: Flashcard)
    func deleteFlashcard(flashcard: Flashcard)

    func openCreateFlashcardPopup()
    func navigateBack()
}

// MARK: View
final class CollectionView: UIViewController {

    // MARK: View Elements
    private lazy var collectionTitleInput: UITextView = {
        let input = UITextView()
        input.font = .boldSystemFont(ofSize: 34)
        input.delegate = titleFieldAdapter
        input.isScrollEnabled = false
        input.translatesAutoresizingMaskIntoConstraints = false
        return input
    }()

    private lazy var createFlashcardButton: UIBarButtonItem = {
        return UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(onCreateFlashcardButtonTap))
    }()

    var titleFieldAdapter: CollectionTitleFieldAdapter!

    var flashcardsListView: UIViewController!
    var output: CollectionViewOutput!

    // MARK: Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .defaultBgColor

        initViews()
        initConstraints()
        initData()

    }

    override func didMove(toParent parent: UIViewController?) {
        guard parent == nil else {
            super.didMove(toParent: parent)
            return
        }

        super.didMove(toParent: parent)
        onBackNavTap()
    }

    // MARK: Init methods
    private func initViews() {
        view.addSubview(collectionTitleInput)

        addChild(flashcardsListView)
        flashcardsListView.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(flashcardsListView.view)
        flashcardsListView.didMove(toParent: self)

        navigationController?.navigationBar.topItem?.backButtonTitle = NSLocalizedString("system_backButtonTitle", comment: "Back")
        navigationItem.rightBarButtonItem = createFlashcardButton

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tapGesture)
    }

    private func initConstraints() {
        let safeArea = view.safeAreaLayoutGuide

        NSLayoutConstraint.activate([
            collectionTitleInput.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: 16),
            collectionTitleInput.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 16),
            collectionTitleInput.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: -16),

            flashcardsListView.view.topAnchor.constraint(equalTo: collectionTitleInput.bottomAnchor, constant: 8),
            flashcardsListView.view.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
            flashcardsListView.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            flashcardsListView.view.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor)
        ])
    }

    private func initData() {
        let collection = output.getCollection()
        collectionTitleInput.text = collection.title
    }

    func showActionsPopup(flashcard: Flashcard) {
        let cancelAction = UIAlertAction(title: NSLocalizedString("system_cancel", comment: "Cancel action"), style: .cancel)
        let showEditModalAction = UIAlertAction(title: NSLocalizedString("system_edit", comment: "Edit action"), style: .default) { _ in
            self.output.openEditFlashcardPopup(flashcard: flashcard)
        }
        let deleteModalAction = UIAlertAction(title: NSLocalizedString("system_delete", comment: "Delete action"), style: .destructive) { _ in
            self.output.deleteFlashcard(flashcard: flashcard)
        }

        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        alert.addAction(showEditModalAction)
        alert.addAction(deleteModalAction)
        alert.addAction(cancelAction)

        present(alert, animated: true)
    }

}

// MARK: Event methods
extension CollectionView {
    @objc private func onCreateFlashcardButtonTap() {
        output.openCreateFlashcardPopup()
    }

    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }

    private func onBackNavTap() {
        output.navigateBack()
    }
}

extension CollectionView: CollectionViewInput {
    func onCollectionTitleEdit() {
        output.editCollectionTitle(title: collectionTitleInput.text)
    }
}
