//
//  CollectionModuleInput.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 02.01.2023.
//

import UIKit

typealias CollectionDismissCallback = (Collection) -> Void

final class CollectionModuleInput: ModuleInput {
    var collection: Collection
    var onDismiss: CollectionDismissCallback

    init(collection: Collection, onDismiss: @escaping CollectionDismissCallback) {
        self.collection = collection
        self.onDismiss = onDismiss
    }
}
