//
//  CollectionRouter.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 02.01.2023.
//

import UIKit

protocol CollectionRouterInput {
    func openFlashcardEditPopup(collection: Collection, flashcard: Flashcard, onDismiss: @escaping FlashcardEditDismissCallback)
    func openFlashcardCreatePopup(collection: Collection, onDismiss: @escaping FlashcardCreateDismissCallback)
}

// MARK: Router
final class CollectionRouter: CollectionRouterInput {
    var navigationController: UINavigationController!

    func openFlashcardEditPopup(collection: Collection, flashcard: Flashcard, onDismiss: @escaping FlashcardEditDismissCallback) {
        let moduleInput = FlashcardEditModuleInput(collection: collection, flashcard: flashcard, onDismiss: onDismiss)
        let module = FlashcardEditModule()
        let viewController = module.create(for: navigationController, moduleInput: moduleInput)
        navigationController.present(viewController, animated: true)
    }

    func openFlashcardCreatePopup(collection: Collection, onDismiss: @escaping FlashcardCreateDismissCallback) {
        let moduleInput = FlashcardCreateModuleInput(collection: collection, onDismiss: onDismiss)
        let module = FlashcardCreateModule()
        let viewController = module.create(for: navigationController, moduleInput: moduleInput)
        navigationController.present(viewController, animated: true)
    }

}
