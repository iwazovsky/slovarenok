//
//  CollectionPresenter.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 02.01.2023.
//

import Foundation

// MARK: Presenter
final class CollectionPresenter {

    weak var view: CollectionViewInput!
    var interactor: CollectionInteractorInput!
    var router: CollectionRouterInput!

    var collection: Collection!
    var onDismiss: CollectionDismissCallback!
    var flashcardsListEditCallback: ((Flashcard) -> Void)!
    var flashcardsListCreateCallback: ((Flashcard) -> Void)!
    var flashcardsListDeleteCallback: ((Flashcard) -> Void)!

    func flashcardEdited(flashcard: Flashcard) {
        guard let index = collection.flashcards.firstIndex(of: flashcard) else {
            print("Index of a flashcard not found")
            return
        }
        var flashcards = collection.flashcards
        flashcards[index] = flashcard
        collection = Collection(id: collection.id, title: collection.title, flashcards: flashcards)

        flashcardsListEditCallback(flashcard)
    }

    func flashcardCreated(flashcard: Flashcard) {
        var flashcards = collection.flashcards
        flashcards.append(flashcard)
        collection = Collection(id: collection.id, title: collection.title, flashcards: flashcards)

        flashcardsListCreateCallback(flashcard)
    }

}

extension CollectionPresenter: CollectionViewOutput {
    func getCollection() -> Collection {
        return collection
    }

    func editCollectionTitle(title: String?) {
        guard let title = title else {
            return
        }
        DispatchQueue.global().async {
            self.interactor.updateCollection(collection: Collection(id: self.collection.id, title: title))
        }
    }

    func deleteFlashcard(flashcard: Flashcard) {
        DispatchQueue.global().async {
            self.interactor.deleteFlashcard(collection: self.collection, flashcard: flashcard)
        }
    }

    // MARK: Navigation
    func openEditFlashcardPopup(flashcard: Flashcard) {
        router.openFlashcardEditPopup(collection: collection, flashcard: flashcard, onDismiss: flashcardEdited)
    }

    func openCreateFlashcardPopup() {
        router.openFlashcardCreatePopup(collection: collection, onDismiss: flashcardCreated)
    }

    func navigateBack() {
        onDismiss(collection)
    }
}

extension CollectionPresenter: CollectionInteractorOutput {
    func flashcardDeleted(collection: Collection, _ flashcard: Flashcard) {
        let flashcards = collection.flashcards.filter {
            $0.id != flashcard.id
        }
        self.collection = Collection(id: collection.id, title: collection.title, flashcards: flashcards)

        DispatchQueue.main.sync {
            flashcardsListDeleteCallback(flashcard)
        }
    }

    func collectionUpdated(_ collection: Collection) {
        self.collection = collection
    }
}
