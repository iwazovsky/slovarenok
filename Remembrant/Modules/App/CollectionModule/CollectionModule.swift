//
//  CollectionModule.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 02.01.2023.
//

import UIKit

final class CollectionModule: Module {

    let moduleOutput: ModuleOutput? = nil

    func create(for navigationController: UINavigationController?, moduleInput: ModuleInput?) -> UIViewController {
        guard let moduleInput = moduleInput as? CollectionModuleInput else {
            fatalError("Wrong module input passed into CollectionConfigurator")
        }

        let viewController = CollectionView()
        let configurator = CollectionConfigurator()
        configurator.configure(viewController: viewController, navigationController: navigationController, moduleInput: moduleInput)

        return viewController
    }
}
