//
//  CollectionInteractor.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 02.01.2023.
//

import Foundation

protocol CollectionInteractorInput {
    func deleteFlashcard(collection: Collection, flashcard: Flashcard)
    func updateCollection(collection: Collection)
}

protocol CollectionInteractorOutput: AnyObject {
    func flashcardDeleted(collection: Collection, _ flashcard: Flashcard)
    func collectionUpdated(_ collection: Collection)
}

// MARK: Interactor
final class CollectionInteractor: CollectionInteractorInput {
    weak var output: CollectionInteractorOutput!
    var flashcardProvider: FlashcardProvider!
    var collectionProvider: CollectionProvider!

    func deleteFlashcard(collection: Collection, flashcard: Flashcard) {
        self.flashcardProvider.deleteFlashcard(collection: collection, flashcard: flashcard) { (inner: DeleteFlashcardThrowableCallback) in
            do {
                try inner()
                self.output.flashcardDeleted(collection: collection, flashcard)
            } catch {
                print(error)
            }
        }
    }

    func updateCollection(collection: Collection) {
        self.collectionProvider.updateCollection(collection) { (inner: UpdateCollectionThrowableCallback) in
            do {
                let collection = try inner()
                self.output.collectionUpdated(collection)
            } catch {
                print(error)
            }
        }
    }
}
