//
//  CollectionConfigurator.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 02.01.2023.
//

import UIKit

// MARK: Configurator
final class CollectionConfigurator: Configurator {

    func configure(viewController: UIViewController, navigationController: UINavigationController?, moduleInput: ModuleInput?, moduleOutput: ModuleOutput?) {
        guard let viewController = viewController as? CollectionView else {
            fatalError("Wrong view passed into CollectionConfigurator")
        }

        guard let moduleInput = moduleInput as? CollectionModuleInput else {
            fatalError("Wrong module input passed into CollectionConfigurator")
        }

        let interactor = CollectionInteractor()

        let flashcardProviderType = String(describing: FlashcardService.self)
        interactor.flashcardProvider = DIContainer.shared.resolve(flashcardProviderType)

        let collectionProviderType = String(describing: CollectionService.self)
        interactor.collectionProvider = DIContainer.shared.resolve(collectionProviderType)

        let router = CollectionRouter()
        router.navigationController = navigationController

        let presenter = CollectionPresenter()
        presenter.view = viewController
        presenter.router = router
        presenter.interactor = interactor
        presenter.collection = moduleInput.collection
        presenter.onDismiss = moduleInput.onDismiss
//

        interactor.output = presenter

        viewController.output = presenter

        let titleFieldAdapter = CollectionTitleFieldAdapter()
        titleFieldAdapter.view = viewController
        viewController.titleFieldAdapter = titleFieldAdapter

        let flashcardsListModule = FlashcardsListModule()
        let flashcardsListModuleInput = FlashcardsListModuleInput(collection: moduleInput.collection, showFlashcardActionsPopup: viewController.showActionsPopup)
        let flashcardsListView = flashcardsListModule.create(for: navigationController, moduleInput: flashcardsListModuleInput)

        viewController.flashcardsListView = flashcardsListView

        guard let moduleOutput = flashcardsListModule.moduleOutput as? FlashcardsListModuleOutput else {
            fatalError("Wrong output in FlashcardsListModule")
        }

        presenter.flashcardsListCreateCallback = moduleOutput.flashcardCreated
        presenter.flashcardsListEditCallback = moduleOutput.flashcardEdited
        presenter.flashcardsListDeleteCallback = moduleOutput.flashcardDeleted
    }

}
