//
//  CollectionTitleFieldAdapter.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 07.01.2023.
//

import UIKit

class CollectionTitleFieldAdapter: NSObject {
    weak var view: CollectionViewInput!

}

extension CollectionTitleFieldAdapter: UITextViewDelegate {
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        view.onCollectionTitleEdit()
        return true
    }
}
