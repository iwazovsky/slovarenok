//
//  Configurator.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 16.11.2022.
//

import UIKit

protocol Configurator {
    func configure(viewController: UIViewController, navigationController: UINavigationController?, moduleInput: ModuleInput?, moduleOutput: ModuleOutput?)
}

extension Configurator {
    func configure(viewController: UIViewController) {
        configure(viewController: viewController, navigationController: nil, moduleInput: nil, moduleOutput: nil)
    }

    func configure(viewController: UIViewController, navigationController: UINavigationController?) {
        configure(viewController: viewController, navigationController: navigationController, moduleInput: nil, moduleOutput: nil)
    }

    func configure(viewController: UIViewController, navigationController: UINavigationController?, moduleInput: ModuleInput?) {
        configure(viewController: viewController, navigationController: navigationController, moduleInput: moduleInput, moduleOutput: nil)
    }
}
