//
//  SceneDelegate.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 05.11.2022.
//

import UIKit

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {

        guard let windowScene = (scene as? UIWindowScene) else { return }

        window = UIWindow(windowScene: windowScene)

        let appConfigurator = AppConfigurator()
        appConfigurator.configure(window: window!)

        window!.makeKeyAndVisible()
    }

}
