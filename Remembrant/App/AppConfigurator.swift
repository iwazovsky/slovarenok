//
//  AppEntry.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 18.11.2022.
//

import UIKit

protocol IAppConfigurator {
    func configure(window: UIWindow)
}

class AppConfigurator: IAppConfigurator {
    func configure(window: UIWindow) {
        DIContainer.shared.initServices()

        let navigationController = UINavigationController()

        let authenticated = true

        if authenticated {
            let module = DashboardModule()
            let viewController = module.create(for: navigationController, moduleInput: nil)

            navigationController.pushViewController(viewController, animated: false)
        } else {
//            let module = SignInModule()
//            let vc = module.create(for: navigationController)

//            let module = SignUpModule()
//            let vc = module.create(for: navigationController)

//            let module = MainTabBarModule()
//            let vc = module.create(for: navigationController)
        }

        window.rootViewController = navigationController
    }
}
