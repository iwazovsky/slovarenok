//
//  DIContainer.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 14.12.2022.
//

import Foundation

final class DIContainer {

    static let shared = DIContainer()

    private lazy var services = [String: Any]()

    private init() {}

    func register<T>(service: T) {
        let key = typeName(T.self)
        services[key] = service
    }

    func resolve<T>() -> T? {
        let key = typeName(T.self)
        return services[key] as? T
    }

    func resolve<T>(_ key: String) -> T? {
        return services[key] as? T
    }

    private func typeName(_ some: Any) -> String {
        return (some is Any.Type) ? "\(some)" : "\(type(of: some))"
    }
}

extension DIContainer {
    func initServices() {

        let jsonHelper = JsonHelper()

        // TODO: Consider moving modelName out of DI
        let coreDataManager = CoreDataManager(modelName: "RemembrantDataModel")

        let apiClient = RemembrantApiClient()
        apiClient.jsonHelper = jsonHelper

        let collectionCacheService = CollectionCacheService()
        collectionCacheService.coreDataManager = coreDataManager
        let collectionNetworkService = CollectionNetworkService()
        collectionNetworkService.jsonHelper = jsonHelper
        collectionNetworkService.client = apiClient

        let collectionService = CollectionService()
        collectionService.cacheProvider = collectionCacheService
        collectionService.networkProvider = collectionNetworkService

        let flashcardCacheService = FlashcardCacheService()
        flashcardCacheService.coreDataManager = coreDataManager
        let flashcardNetworkService = FlashcardNetworkService()
        flashcardNetworkService.jsonHelper = jsonHelper
        flashcardNetworkService.client = apiClient

        let flashcardService = FlashcardService()
        flashcardService.cacheProvider = flashcardCacheService
        flashcardService.networkProvider = flashcardNetworkService

        DIContainer.shared.register(service: GreetingRandomizer())
        DIContainer.shared.register(service: jsonHelper)
        DIContainer.shared.register(service: collectionService)
        DIContainer.shared.register(service: flashcardService)
    }
}
