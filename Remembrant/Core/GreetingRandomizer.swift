//
//  GreetingsRandomizer.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 14.12.2022.
//

import Foundation

final class GreetingRandomizer {
    private let greetings = [
        "Welcome", // english
        "Добро пожаловать", // russian
        "Ласкаво просимо", // ukranian
        "Вітаем", // belorussian
        "Bienvenidos", // spanish
        "स्वागत हे", // hindi
        "Bem-vindo", // portugese
        "ようこそ", // japanese
        "Willkommen", // german
        "欢迎", // chinese
        "Bonjour" // french
    ]

    func getRandomizedGreeting() -> String {
        return greetings[Int.random(in: 0...greetings.count-1)]
    }
}
