//
//  DashboardShapeView.swift
//  Remembrant
//
//  Created by Konstantin Tukmakov on 10.01.2023.
//

import UIKit

// MARK: View
final class DashboardPillView: UIView {

    // MARK: Lifecycle methods
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder: NSCoder) {
        fatalError("Storyboards are not supported")
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)

        let fillColor = UIColor.primaryColor

        fillColor.setFill()
//        UIRectFill(rect)

        let path = getPath(rect)
        path.fill()

//        UIColor.red.setStroke()
//        path.stroke()
        clipsToBounds = true
    }

    // MARK: Create path
    func getPath(_ rect: CGRect) -> UIBezierPath {
        let path = UIBezierPath()

        path.move(to: CGPoint(x: rect.maxX*0.22, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.maxX*0.78, y: rect.minY))

        path.addCurve(
            to: CGPoint(x: rect.maxX*0.78, y: rect.maxY),
            controlPoint1: CGPoint(x: rect.maxX, y: rect.minY),
            controlPoint2: CGPoint(x: rect.maxX, y: rect.maxY)
        )

        path.addLine(to: CGPoint(x: rect.maxX*0.22, y: rect.maxY))
        path.addCurve(
            to: CGPoint(x: rect.maxX*0.22, y: rect.minY),
            controlPoint1: CGPoint(x: rect.minX, y: rect.maxY),
            controlPoint2: CGPoint(x: rect.minX, y: rect.minY)
        )

        path.close()

        return path
    }
}
