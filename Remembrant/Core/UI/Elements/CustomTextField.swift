//
//  CustomTextField.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 08.01.2023.
//

import UIKit

class CustomTextField: UITextField {
    let padding = UIEdgeInsets(top: 12, left: 20, bottom: 12, right: 20)

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
