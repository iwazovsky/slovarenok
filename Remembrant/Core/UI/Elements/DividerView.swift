//
//  DividerView.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 08.01.2023.
//

import UIKit

class DividerView: UIView {
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath()

        path.move(to: CGPoint(x: rect.minX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
        path.close()

        UIColor.primaryColor.set()
        path.lineWidth = 1
        path.stroke()
    }
}
