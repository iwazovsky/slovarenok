//
//  UILabel+Typography.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 03.01.2023.
//

import UIKit

extension UILabel {
    static let superBigSize: CGFloat = 60
    static let titleSize: CGFloat = 34
    static let secondaryTitleSize: CGFloat = 17
    static let bodySize: CGFloat = 17
    static let secondarySize: CGFloat = 15
    static let captionsSize: CGFloat = 13
    static let smallCaptionsSize: CGFloat = 11
}
