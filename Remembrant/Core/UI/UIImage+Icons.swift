//
//  UIImage+Icons.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 02.01.2023.
//

import UIKit

extension UIImage {
    static let expandMoreIcon: UIImage? = {
        return UIImage(named: "ExpandMoreIcon")
    }()

    static let expandLessIcon: UIImage? = {
        return UIImage(named: "ExpandLessIcon")
    }()

    static let moreMenuIcon: UIImage? = {
        return UIImage(named: "MoreMenuIcon")
    }()

    static let arrowForwardIcon: UIImage? = {
        return UIImage(named: "ArrowForwardIcon")
    }()
}
