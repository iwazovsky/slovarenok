//
//  UIColor+Extension.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 17.11.2022.
//

import UIKit

extension UIColor {

    static let primaryColor = UIColor(red: 0.706, green: 0.475, blue: 1, alpha: 1)
    static let accentColor = UIColor(red: 0.965, green: 0.216, blue: 0.925, alpha: 1)
    static let color3 = UIColor(red: 0.980, green: 0.918, blue: 0.282, alpha: 1)
    static let color4 = UIColor(red: 0.984, green: 0.706, blue: 0.329, alpha: 1)

    static let defaultBgColor: UIColor = {
        if #available(iOS 13.0, *) {
            return UIColor { (traits) -> UIColor in
                return traits.userInterfaceStyle == .dark ? UIColor(red: 0, green: 0, blue: 0, alpha: 1) : UIColor(red: 1, green: 1, blue: 1, alpha: 1)

            }
        } else {
            return UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        }
    }()

    static let defaultModalBgColor: UIColor = {
        if #available(iOS 13.0, *) {
            return UIColor { (traits) -> UIColor in
                return traits.userInterfaceStyle == .dark ? UIColor(red: 0.11, green: 0.11, blue: 0.118, alpha: 1) : UIColor(red: 0.949, green: 0.945, blue: 0.969, alpha: 1)
            }
        } else {
            return UIColor(red: 0.949, green: 0.945, blue: 0.969, alpha: 1)
        }
    }()

    static let inputBgColor: UIColor = {
        if #available(iOS 13.0, *) {
            return UIColor { (traits) -> UIColor in
                return traits.userInterfaceStyle == .dark ? UIColor(red: 0.15, green: 0.15, blue: 0.15, alpha: 1) : UIColor(red: 1, green: 1, blue: 1, alpha: 1)
            }
        } else {
            return UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        }
    }()

    static let primaryTextColor: UIColor = {
        if #available(iOS 13.0, *) {
            return UIColor { (traits) -> UIColor in
                return traits.userInterfaceStyle == .dark ? UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1) : UIColor(red: 0.05, green: 0.05, blue: 0.05, alpha: 1)
            }
        } else {
            return UIColor(red: 0.01, green: 0.01, blue: 0.01, alpha: 1)
        }
    }()

    static let secondaryTextColor: UIColor = {
        if #available(iOS 13.0, *) {
            return UIColor { (traits) -> UIColor in
                return traits.userInterfaceStyle == .dark ? UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1) : UIColor(red: 0.15, green: 0.15, blue: 0.15, alpha: 1)
            }
        } else {
            return UIColor(red: 0.15, green: 0.15, blue: 0.15, alpha: 1)
        }
    }()
}
