//
//  UILabel+UpdateAttachments.swift
//  Slovarenok
//
//  Created by Konstantin Tukmakov on 03.01.2023.
//

import UIKit

@available(iOS 13.0, *)
extension UILabel {

    func updateAttachments() {
        guard let attributedString = attributedText else { return }
        let mutableAttributedString = NSMutableAttributedString(attributedString: attributedString)
        attributedString.enumerateAttribute(.attachment, in: .init(location: 0, length: attributedString.string.utf16.count), options: []) { value, range, _ in
            guard let attachment = value as? NSTextAttachment else { return }
            guard let image = attachment.image else { return }
            guard let asset = image.imageAsset else { return }
            attachment.image = asset.image(with: .current)
            mutableAttributedString.setAttributes([.attachment: attachment], range: range)
        }

        attributedText = mutableAttributedString
    }

}
